import fun as fn


def create_csv():
  with open(fn.PATH + '/rus_climat.csv', 'w', newline='', encoding='utf-8') as f:
    writer = fn.csv.writer(f, delimiter=';')
    writer.writerow((
        'pagetitle',
        'article',
        'description',
        'gallery',
        'parent',
        'price',
        'power_hot',
        'color',
        'bright',
        'termostat',
        'pdu',
        'size',
        'weight',
        'size_d',
        'size_w',
        'size_h',
        'video_link',
        'parser',
        'alias',
        'template',
        'image',
        'published',
        'vendor',
        'color_id'
    ))


def write_csv(data):
  with open(fn.PATH + '/rus_climat.csv', 'a', newline='', encoding='utf-8') as f:

    writer = fn.csv.writer(f, delimiter=';')
    writer.writerow((
        data['title'],
        data['article'],
        data['description'],
        data['gallery'],
        data['category'],
        data['price'],
        data['power_hot'],
        data['color'],
        data['bright'],
        data['termostat'],
        data['pdu'],
        data['size'],
        data['weight'],
        data['size_d'],
        data['size_w'],
        data['size_h'],
        data['video_link'],
        'rusclimat',
        data['alias'],
        data['template'],
        data['image'],
        data['published'],
        data['vendor'],
        data['color_id']
    ))


def change_csv(old, new):
  if fn.path.isfile(new):
    fn.remove(new)
  if fn.path.isfile(old):
    fn.rename(old, new)


def rusklimat():

  base_site = 'https://www.rusklimat.com'
  html = fn.get_html(
      'https://www.rusklimat.com/auth/login.jsp?ID=login&loginkey=1&url=/price/html/c-1318-s-0.html&login=info%40kamin24.ru&password=d58341b')

  if html == False:
    return False

  soup = fn.BeautifulSoup(html, 'html.parser')
  list = soup.find_all('table', class_='table')

  links = []
  data = {}
  # переименовываем старый файл
  if fn.path.isfile(fn.PATH + '/rus_climat.csv'):
    change_csv(fn.PATH + '/rus_climat.csv', fn.PATH + '/rus_climat_prev.csv')
  # создаем новый файл csv
  create_csv()

  for table in list:
    try:
      for tr in table.find_all('tr'):
        try:
          # берем только первый td
          for url in tr.find('td'):
            links.append(url.get('href')+'|'+url.text)
        except:
          pass
    except:
      pass

  for link in links:
    link_re = link.split('|')

    cart_html = fn.get_html(base_site + link_re[0])

    if cart_html == False:
      continue

    soup = fn.BeautifulSoup(cart_html, 'html.parser')

    # название
    try:
      data['title'] = soup.find('h2').text
    except:
      data['title'] = ''

    # артикул
    try:
      data['article'] = link_re[1]
    except:
      data['article'] = ''

    # алиас
    try:
      data['alias'] = fn.translate(
          data['title']) + fn.translate(data['article'])
    except:
      data['alias'] = ''  # все равно товар не добавится в базу если нет этого поля

    # описание
    try:
      description_html = soup.find('div', class_='brand-description')
      data['description'] = ''
      for child in description_html.children:
        if('price-outer' in str(child)):
          break
        data['description'] += str(child).strip()
    except:
      data['description'] = ''

    # фото
    try:
      images_html = soup.find(
          'div', class_='product-photos-platform').find_all('a')
      images = []
      for image in images_html:
        images.append(image.get('href'))
      data['image'] = images[0]
      data['gallery'] = []
      data['gallery'] = '|'.join(images)
    except:
      data['gallery'] = ''

    if not data['image'] and not data['gallery']:
      continue

    # категория
    try:
      category_name = str(soup.find('p', class_="path").find_all('a')[-1].text)

      # обозначаем категории
      category_id = 0

      if category_name == 'Мини-камины':
        category_id = 308
      if category_name == 'Напольные камины':
        category_id = 1717
      if category_name == 'Настенные камины':
        category_id = 1718
      if category_name == 'Каминокомплекты':
        category_id = 2276
      if category_name == 'Очаги для портальных каминов':
        category_id = 302
      if category_name == 'Порталы':
        category_id = 304

      if category_id == 0:
        category_id = 26

      data['category'] = category_id
    except:
      data['category'] = ''

    # цена
    try:
      # Рекомендуемая розничная цена
      data['price'] = str(
          soup.find('b', class_="price").next_element.replace(' ', ''))
    except:
      data['price'] = 0

    # характеристики
    try:
      features_html = soup.select('#tech-tab td')

      list_key = [fn.re.sub(r'<[^>]*?>', '', str(x))
                  for x in features_html[::2]]
      list_value = [fn.re.sub(r'<[^>]*?>', '', str(x))
                    for x in features_html[1::2]]

      data['power_hot'] = ''
      data['color'] = ''
      data['color_id'] = ''
      data['bright'] = ''
      data['termostat'] = ''
      data['pdu'] = ''
      data['size'] = ''
      data['weight'] = ''
      data['size_d'] = ''
      data['size_w'] = ''
      data['size_h'] = ''

      for idx, f in enumerate(list_key):
        lv = list_value[idx].strip()
        if f == 'Мощность':
          data['power_hot'] = lv if lv else ''
        if f == 'Цвет':
          data['color'] = "|".join(lv.split(', ')) if lv else ''
          data['color_id'] = lv if lv else 'Не задан'

        if f == 'Регулировка яркости пламени':
          data['bright'] = 1 if lv else ''
        if f == 'Наличие термостата':
          data['termostat'] = 1 if lv else ''
        if f == 'Пульт управления':
          data['pdu'] = 1 if lv else ''
        if 'Размеры прибора' in f or 'Габариты' in f:
          data['size'] = lv.replace(' мм', '').replace('х', 'x') if lv else ''
          
          
        # if f == 'Гарантия': features['power_hot'] = lv if lv else ''
        if f == 'Вес нетто, кг':
          data['weight'] = lv if lv else ''
        # if f == 'Вес брутто, кг': features[''] = lv if lv else ''
        # if f == 'Объём упаковки товара, м3': features[''] = lv if lv else ''

        if f == 'Примечание':
          data['description'] += "<br /><b>Примечание:</b> "+lv

    except:
      pass

    if data['size']:
      list_size = data['size'].split('x')
      data['size_w'] = list_size[0]
      data['size_d'] = list_size[1]
      data['size_h'] = list_size[2]

    # видео
    try:
      data['video_link'] = soup.find('div', class_="content").find('iframe').get('src').replace(
          'https://www.youtube.com/embed/', '').replace('http://www.youtube.com/embed/', '')
    except:
      data['video_link'] = ''

    data['vendor'] = 1002
    data['published'] = 1
    data['template'] = 7
  

    # добавляем в csv файл
    write_csv(data)
