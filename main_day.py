import fun as fn
import pech_kratki


def backup_csv(donor):
    # переименовываем старый файл
    if fn.path.isfile(fn.PATH + '/{}.csv'.format(donor)):
        pech_kratki.change_csv(fn.PATH + '/{}.csv'.format(donor),
                        fn.PATH + '/{}_prev.csv'.format(donor))


def main():

    print('парсим pech_kratki')
    backup_csv('pech_kratki')
    pech_kratki.run()

    print('парсить закончили')


if __name__ == '__main__':
    main()
