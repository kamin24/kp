import fun as fn
import mosng


def main():

    if fn.path.isfile(fn.PATH + '/mosng.csv'):
        mosng.change_csv(fn.PATH + '/mosng.csv', fn.PATH + '/mosng_prev.csv')
    mosng.create_csv()
    mosng.run()
    print('парсить mosng закончили')


if __name__ == '__main__':
    main()
