import fun as fn


def create_csv():
    with open(fn.PATH + '/belfortkamin.csv', 'w', newline='', encoding='utf-8') as f:
        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow((
            'title',
            'parent',
            'article',
            'alias',
            'price',
            'old_price',
            'content',
            'gallery',
            'images',
            'dop_info',
            'bfk_coil',
            'bfk_open_doors',
            'bfk_forma_doors',
            'bfk_type_gas',
            'bfk_design',
            'bfk_option',
            'bfk_location',
            'bfk_option_bio',
            'bfk_type_bio',
            'bfk_type_id',
            'pdu',
            'material',
            'power_hot',
            'size_w',
            'weight',
            'color_id',
            'color',
            'published',
            'template',
            'parser',
            'vendor',
            'size_d',
            'size_h',

            'bfk_kpd',
            'bfk_artikul_proizvoditelya',
            'bfk_opcii_kaminov_i_pechey',
            'bfk_tip_ustroystva',
            'bfk_cvet_reshetki',
            'bfk_osnovnye_cveta_ili_faktura_obramleniya',
            'bfk_raspolojenie_biokamina_v_pomeshchenii',
            'bfk_obem_biokonteynera',
            'bfk_shirina_kaminokomplekta_mm',
            'bfk_moshchnost_v_vodu_kvt',
            'bfk_maksimalnoe_vhodnoe_davlenie_mbar',
            'bfk_vysota_kaminokomplekta_mm',
            'bfk_vid',
            'bfk_nalichie_balki',
            'bfk_distancionnoe_upravleniepult',
            'bfk_dlina_polosy_ognya_v_odnom_biokonteynere',
            'bfk_pokrytie',
            'bfk_nalichie_karmana',
            'bfk_vysota_vidimoy_chasti_topki_mm',
            'bfk_regulirovka_vozdushnogo_potoka',
            'bfk_shirina_vidimoy_chasti_topki_mm',
            'bfk_nalichie_setki',
            'bfk_nalichie_dymosbornika',
            'bfk_tip_dop_oborudovaniya',
            'bfk_material_izgotovleniya_topki',
            'bfk_naznachenie',
            'bfk_tip_ispolneniya',
            'bfk_minimalnoe_vhodnoe_davlenie_mbar',
            'bfk_kolichestvo_biokonteynerov',
            'bfk_futerovka',
            'bfk_diametr_dymohoda_mm',
            'bfk_nalichie_zolnika',
            'bfk_material_oblicovki',
            'bfk_maksimalnaya_dlina_drov_mm',
            'bfk_material_izgotovleniya_biokamina',
            'bfk_cvet_otdelki',
            'bfk_nominmoshchnost_kvt',
            'bfk_razmery_gabar_sm__ustan_mm',
            'bfk_vid_topliva',
            'bfk_tip_nagrevaniya',
            'bfk_glubina_gabarit_bez_ruchki_mm',
            'bfk_prisoedinenie_dymohoda',
            'bfk_nom_moshchnost_kvt',
            'bfk_nominalnoe_vhodnoe_davlenie_mbar',
            'bfk_otaplivaemyy_obem',
        ))


def write_csv(data):
    with open(fn.PATH + '/belfortkamin.csv', 'a', newline='', encoding='utf-8') as f:

        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow((
            data['title'],
            data['parent'],
            data['article'],
            data['alias'],
            data['price'],
            data['old_price'],
            data['content'],
            data['gallery'],
            data['images'],
            data['dop_info'],
            data['bfk_coil'],
            data['bfk_open_doors'],
            data['bfk_forma_doors'],
            data['bfk_type_gas'],
            data['bfk_design'],
            data['bfk_option'],
            data['bfk_location'],
            data['bfk_option_bio'],
            data['bfk_type_bio'],
            data['bfk_type_id'],
            data['pdu'],
            data['material'],
            data['power_hot'],
            data['size_w'],
            data['weight'],
            data['color_id'],
            data['color'],
            1,
            7,
            'belfortkamin',
            data['vendor'],
            data['size_d'],
            data['size_h'],

            data['bfk_kpd'],
            data['bfk_artikul_proizvoditelya'],
            data['bfk_opcii_kaminov_i_pechey'],
            data['bfk_tip_ustroystva'],
            data['bfk_cvet_reshetki'],
            data['bfk_osnovnye_cveta_ili_faktura_obramleniya'],
            data['bfk_raspolojenie_biokamina_v_pomeshchenii'],
            data['bfk_obem_biokonteynera'],
            data['bfk_shirina_kaminokomplekta_mm'],
            data['bfk_moshchnost_v_vodu_kvt'],
            data['bfk_maksimalnoe_vhodnoe_davlenie_mbar'],
            data['bfk_vysota_kaminokomplekta_mm'],
            data['bfk_vid'],
            data['bfk_nalichie_balki'],
            data['bfk_distancionnoe_upravleniepult'],
            data['bfk_dlina_polosy_ognya_v_odnom_biokonteynere'],
            data['bfk_pokrytie'],
            data['bfk_nalichie_karmana'],
            data['bfk_vysota_vidimoy_chasti_topki_mm'],
            data['bfk_regulirovka_vozdushnogo_potoka'],
            data['bfk_shirina_vidimoy_chasti_topki_mm'],
            data['bfk_nalichie_setki'],
            data['bfk_nalichie_dymosbornika'],
            data['bfk_tip_dop_oborudovaniya'],
            data['bfk_material_izgotovleniya_topki'],
            data['bfk_naznachenie'],
            data['bfk_tip_ispolneniya'],
            data['bfk_minimalnoe_vhodnoe_davlenie_mbar'],
            data['bfk_kolichestvo_biokonteynerov'],
            data['bfk_futerovka'],
            data['bfk_diametr_dymohoda_mm'],
            data['bfk_nalichie_zolnika'],
            data['bfk_material_oblicovki'],
            data['bfk_maksimalnaya_dlina_drov_mm'],
            data['bfk_material_izgotovleniya_biokamina'],
            data['bfk_cvet_otdelki'],
            data['bfk_nominmoshchnost_kvt'],
            data['bfk_razmery_gabar_sm__ustan_mm'],
            data['bfk_vid_topliva'],
            data['bfk_tip_nagrevaniya'],
            data['bfk_glubina_gabarit_bez_ruchki_mm'],
            data['bfk_prisoedinenie_dymohoda'],
            data['bfk_nom_moshchnost_kvt'],
            data['bfk_nominalnoe_vhodnoe_davlenie_mbar'],
            data['bfk_otaplivaemyy_obem'],
        ))


def change_csv(old, new):
    if fn.path.isfile(new):
        fn.remove(new)
    if fn.path.isfile(old):
        fn.rename(old, new)

NAME = []

def run(url):

    print('run: ' + url)

    URL = "https://belfortkamin.ru"
    html = fn.get_html(url)
    category_content = fn.BeautifulSoup(html, 'html.parser')

    
    
    data = {}


    list_item = category_content.find(class_="products-category").findAll(class_="product-layout")

    for item in list_item:

        item_link = URL + str(item.find("a").get("href"))
        product_html = fn.get_html(item_link)

        if product_html == False:
            continue

        soup = fn.BeautifulSoup(product_html, 'html.parser')

        try:
            data['title'] = soup.find('h1').text.strip()
        except:
            data['title'] = 'Камин без названия'

        data['parent'] = 26

        # категории
        if 'https://belfortkamin.ru/category/fire-chambers/' in url or 'https://belfortkamin.ru/category/Fire-chambers/' in url:
            data['parent'] = 5142
        elif 'https://belfortkamin.ru/category/gazovye-topki/' in url:
            data['parent'] = 5143
        elif 'https://belfortkamin.ru/category/pechi-kaminy-kaminokomlekty/' in url:
            data['parent'] = 5144
        elif 'https://belfortkamin.ru/category/biokaminy/' in url:
            data['parent'] = 7283
            
            if 'контейнер' in data['title'].lower():
                data['parent'] = 7284

        elif 'https://belfortkamin.ru/category/oblicovki-dlja-topok/' in url:
            data['parent'] = 304
        elif 'https://belfortkamin.ru/category/kaminnye-reshetki/' in url:
            data['parent'] = 6033
        elif 'https://belfortkamin.ru/category/opcii_dlya_kaminov/' in url:
            data['parent'] = 301
        elif 'https://belfortkamin.ru/category/discount/' in url:
            data['parent'] = 6034

        

        # фото
        # устраняем недостаток проблема между тегами на доноре
        # zoom_gallery = soup.find(class_='image-additional')
        # print(zoom_gallery)
        # return

        images = soup.find(class_='image-additional').find_all('a', class_='thumbnail')

        list_images = []
        if images:
            for image in images:
                imgg = ''
                imgg = image.get('data-zoom-image')
                if imgg is not None:
                    list_images.append(URL + image.get('data-zoom-image'))

        data['gallery'] = '|'.join(list_images)
        data['images'] = list_images[0]

        productID = soup.find(
                'input', {"name":"product_id"}).get('value')
        try:
            data['article'] = soup.find(
                'span', class_='hint').text.replace('арт. ', '').strip()
        except:
            data['article'] = str(fn.hashlib.md5(data['title'].encode(
                    'utf-8')).hexdigest()[:6])

        
        
        data['article'] = "BFK-{}-{}".format(str(data['article'])[:6], productID).upper()
        
        data['productID'] = productID

        try:
            data['alias'] = 'BFK-' + fn.translate(data['title']) + fn.translate(
                data['article']).replace('--', '-').replace('/', '-').replace(',', '-').replace(')', '-').replace('(', '-')
        except:
            data['alias'] = ''


        try:
            data['price'] = soup.find(class_='add2cart').find("span", class_="price-new").get("data-price")
        except:
            data['price'] = 0

        # это наверное комплектом продается
        if(data['price'] == 0): continue 

        data['old_price'] = 0 # вроде нет старой цены

        try:
            data['content'] = soup.find(class_='cpt_product_description')
        except:
            data['content'] = ''

        try:
            attributes = soup.find("div", {"id":"tab-features"}).find('table', class_='table')
        except:
            attributes = ''

        data['dop_info'] = str(attributes)


        if attributes is not None:

            
            data['pdu'] = ''
            data['material'] = ''
            data['power_hot'] = ''
            data['size_w'] = ''
            data['weight'] = ''
            data['color_id'] = ''
            data['color'] = ''
            data['vendor'] = ''
            data['size_d'] = ''
            data['size_h'] = ''

            data['bfk_coil'] = ''
            data['bfk_open_doors'] = ''
            data['bfk_forma_doors'] = ''
            data['bfk_type_gas'] = ''
            data['bfk_design'] = ''
            data['bfk_option'] = ''
            data['bfk_location'] = ''
            data['bfk_option_bio'] = ''
            data['bfk_type_bio'] = ''
            data['bfk_type_id'] = ''
			
            data['bfk_artikul_proizvoditelya'] = ''
            data['bfk_opcii_kaminov_i_pechey'] = ''
            data['bfk_tip_ustroystva'] = ''
            data['bfk_cvet_reshetki'] = ''
            data['bfk_osnovnye_cveta_ili_faktura_obramleniya'] = ''
            data['bfk_raspolojenie_biokamina_v_pomeshchenii'] = ''
            data['bfk_obem_biokonteynera'] = ''
            data['bfk_shirina_kaminokomplekta_mm'] = ''
            data['bfk_moshchnost_v_vodu_kvt'] = ''
            data['bfk_maksimalnoe_vhodnoe_davlenie_mbar'] = ''
            data['bfk_vysota_kaminokomplekta_mm'] = ''
            data['bfk_vid'] = ''
            data['bfk_nalichie_balki'] = ''
            data['bfk_distancionnoe_upravleniepult'] = ''
            data['bfk_dlina_polosy_ognya_v_odnom_biokonteynere'] = ''
            data['bfk_pokrytie'] = ''
            data['bfk_nalichie_karmana'] = ''
            data['bfk_vysota_vidimoy_chasti_topki_mm'] = ''
            data['bfk_regulirovka_vozdushnogo_potoka'] = ''
            data['bfk_shirina_vidimoy_chasti_topki_mm'] = ''
            data['bfk_nalichie_setki'] = ''
            data['bfk_nalichie_dymosbornika'] = ''
            data['bfk_tip_dop_oborudovaniya'] = ''
            data['bfk_material_izgotovleniya_topki'] = ''
            data['bfk_naznachenie'] = ''
            data['bfk_tip_ispolneniya'] = ''
            data['bfk_minimalnoe_vhodnoe_davlenie_mbar'] = ''
            data['bfk_kolichestvo_biokonteynerov'] = ''
            data['bfk_futerovka'] = ''
            data['bfk_diametr_dymohoda_mm'] = ''
            data['bfk_nalichie_zolnika'] = ''
            data['bfk_material_oblicovki'] = ''
            data['bfk_maksimalnaya_dlina_drov_mm'] = ''
            data['bfk_material_izgotovleniya_biokamina'] = ''
            data['bfk_cvet_otdelki'] = ''
            data['bfk_nominmoshchnost_kvt'] = ''
            data['bfk_razmery_gabar_sm__ustan_mm'] = ''
            data['bfk_vid_topliva'] = ''
            data['bfk_tip_nagrevaniya'] = ''
            data['bfk_glubina_gabarit_bez_ruchki_mm'] = ''
            data['bfk_prisoedinenie_dymohoda'] = ''
            data['bfk_nom_moshchnost_kvt'] = ''
            data['bfk_nominalnoe_vhodnoe_davlenie_mbar'] = ''
            data['bfk_otaplivaemyy_obem'] = ''
            data['bfk_kpd'] = ''

            for tr in attributes.find_all('tr'):
                tds = tr.find_all('td')
                if tds:
                    lv = tds[1].text
                    tn = tds[0].text

                    if 'Теплообменник' in tn: data['bfk_coil'] = 1 if lv == 'Да' else ''
                                       
                    elif 'КПД' in tn: data['bfk_kpd'] = lv if lv else ''    
                    elif 'Артикул производителя' in tn: data['bfk_artikul_proizvoditelya'] = lv if lv else ''
                    elif 'Опции каминов и печей' in tn: data['bfk_opcii_kaminov_i_pechey'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Тип устройства' in tn: data['bfk_tip_ustroystva'] = lv if lv else ''
                    elif 'Расположение биокамина в помещении' in tn: data['bfk_raspolojenie_biokamina_v_pomeshchenii'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Объем биоконтейнера' in tn: data['bfk_obem_biokonteynera'] = lv if lv else ''
                    elif 'Ширина каминокомплекта, мм' in tn: 
                        data['size_w'] = lv if data['size_w'] == '' else data['size_w'] 
                        data['bfk_shirina_kaminokomplekta_mm'] = lv if lv else ''
                    elif 'Мощность в воду, кВт' in tn: data['bfk_moshchnost_v_vodu_kvt'] = lv if lv else ''
                    elif 'Максимальное входное давление, мбар' in tn: data['bfk_maksimalnoe_vhodnoe_davlenie_mbar'] = lv if lv else ''
                    elif 'Высота каминокомплекта, мм' in tn: 
                        data['size_h'] = lv if data['size_h'] == '' else data['size_h'] 
                        data['bfk_vysota_kaminokomplekta_mm'] = lv if lv else ''
                    elif 'Вид' == tn: data['bfk_vid'] = lv if lv else ''
                    elif 'Наличие балки' in tn: data['bfk_nalichie_balki'] = lv if lv else ''
                    elif 'Дистанционное управление(пульт)' in tn: data['bfk_distancionnoe_upravleniepult'] = 1 if lv == 'Да' else ''
                    elif 'Длина полосы огня в одном биоконтейнере' in tn: data['bfk_dlina_polosy_ognya_v_odnom_biokonteynere'] = lv if lv else ''
                    elif 'Покрытие' in tn: data['bfk_pokrytie'] = lv if lv else ''
                    elif 'Наличие "кармана"' in tn: data['bfk_nalichie_karmana'] = lv if lv else ''
                    elif 'Высота видимой части топки, мм' in tn: 
                        data['size_h'] = lv if data['size_h'] == '' else data['size_h'] 
                        data['bfk_vysota_vidimoy_chasti_topki_mm'] = lv if lv else ''
                    elif 'Регулировка воздушного потока' in tn: data['bfk_regulirovka_vozdushnogo_potoka'] = lv if lv else ''
                    elif 'Ширина видимой части топки (мм)' in tn: 
                        data['size_w'] = lv if data['size_w'] == '' else data['size_w'] 
                        data['bfk_shirina_vidimoy_chasti_topki_mm'] = lv if lv else ''
                    elif 'Наличие сетки' in tn: data['bfk_nalichie_setki'] = lv if lv else ''
                    elif 'Наличие дымосборника' in tn: data['bfk_nalichie_dymosbornika'] = lv if lv else ''
                    elif 'Тип доп. оборудования' in tn: data['bfk_tip_dop_oborudovaniya'] = lv if lv else ''
                    elif 'Материал изготовления топки' in tn: 
                        data['material'] = "|".join(lv.split(', ')) if lv else ''
                        data['bfk_material_izgotovleniya_topki'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Назначение' in tn: data['bfk_naznachenie'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Тип исполнения' in tn: data['bfk_tip_ispolneniya'] = lv if lv else ''
                    elif 'Минимальное входное давление, мбар' in tn: data['bfk_minimalnoe_vhodnoe_davlenie_mbar'] = lv if lv else ''
                    elif 'Количество биоконтейнеров' in tn: data['bfk_kolichestvo_biokonteynerov'] = lv if lv else ''
                    elif 'Футеровка' in tn: data['bfk_futerovka'] = lv if lv else ''
                    elif 'Диаметр дымохода, мм' in tn: data['bfk_diametr_dymohoda_mm'] = lv if lv else ''
                    elif 'Наличие зольника' in tn: data['bfk_nalichie_zolnika'] = lv if lv else ''
                    elif 'Материал облицовки' in tn: data['bfk_material_oblicovki'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Максимальная длина дров, мм' in tn: data['bfk_maksimalnaya_dlina_drov_mm'] = lv if lv else ''
                    elif 'Материал изготовления биокамина' in tn: data['bfk_material_izgotovleniya_biokamina'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Цвет отделки' in tn: data['bfk_cvet_otdelki'] = lv if lv else ''
                    elif 'Номин.мощность, кВт' in tn: data['bfk_nominmoshchnost_kvt'] = lv if lv else ''
                    elif 'Размеры: габар., см / устан., мм' in tn: data['bfk_razmery_gabar_sm__ustan_mm'] = lv if lv else ''
                    elif 'Вид топлива' == tn: data['bfk_vid_topliva'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Тип нагревания' in tn: data['bfk_tip_nagrevaniya'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Глубина (габарит без ручки), мм' in tn: 
                        data['size_d'] = lv if data['size_d'] == '' else data['size_d']
                        data['bfk_glubina_gabarit_bez_ruchki_mm'] = lv if lv else ''
                    elif 'Присоединение дымохода' in tn: data['bfk_prisoedinenie_dymohoda'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Ном. мощность, кВт' in tn: data['bfk_nom_moshchnost_kvt'] = lv if lv else ''
                    elif 'Номинальное входное давление, мбар' in tn: data['bfk_nominalnoe_vhodnoe_davlenie_mbar'] = lv if lv else ''
                    elif 'Отапливаемый объём' in tn: data['bfk_otaplivaemyy_obem'] = lv if lv else ''
                    elif 'Цвет решетки' in tn: data['bfk_cvet_reshetki'] = lv if lv else ''
                    elif 'Основные цвета или фактура обрамления' in tn: data['bfk_osnovnye_cveta_ili_faktura_obramleniya'] = lv if lv else ''


                    elif 'Способ открытия дверцы' in tn: data['bfk_open_doors'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Форма стекла и дверей' in tn: data['bfk_forma_doors'] = lv if lv else ''
                    elif 'Тип газа' in tn: data['bfk_type_gas'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Дизайн и исполнение' in tn: data['bfk_design'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Опции биокаминов' in tn: data['bfk_option_bio'] = "|".join(lv.split(', ')) if lv else ''
                    elif tn == 'Опции': data['bfk_option'] = "|".join(lv.split(', ')) if lv else ''

                    elif 'Расположение в помещении' in tn: data['bfk_location'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Тип биокамина' in tn: data['bfk_type_bio'] = lv if lv else ''
                    
                    elif 'Материал изготовления' in tn: data['material'] = "|".join(lv.split(', ')) if lv else ''
                    elif 'Ширина видимой' in tn: data['size_w'] = lv if lv else ''
                    elif 'Вес, кг' in tn: data['weight'] = lv.replace(' кг', '').strip() if lv else '0'
                    elif 'Ширина' in tn: data['size_w'] = lv if lv else ''
                    elif 'Высота' in tn: data['size_h'] = lv if lv else ''
                    elif 'Глубина' in tn: data['size_d'] = lv if lv else ''
                    elif 'Основные цвета' in tn or 'Цвет решетки' in tn or tn == 'Цвет':
                        data['color_id'] = lv if lv else ''
                        data['color'] = "|".join(lv.split(', ')) if lv else ''

                    # brand
                    if 'Бренд' in tn:
                        if 'Kratki' in lv: data['vendor'] = 1009
                        elif 'ABX' in lv: data['vendor'] = 1010
                        elif 'Nordpeis' in lv: data['vendor'] = 1008
                        elif 'FireBird' in lv: data['vendor'] = 1011
        
        write_csv(data)
    

    try:
        next = category_content.find("ul", class_="pagination").find_all('a')[-1]

        if 'inline-link' in next.get("class"):
            run(URL + next.get('href'))

    except:
        pass

    