import fun as fn


def create_csv():
    with open(fn.PATH + '/electrokamin4u.csv', 'w', newline='', encoding='utf-8') as f:
        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow((
            'title',
            'parent',
            'article',
            'alias',
            'price',
            'content',
            'video_link',
            'gallery',
            'images',
            'dop_info',
            'type_id',
            'size',
            'hearth',
            'type_hearth',
            'drova',
            'bright',
            'mute_drov',
            'flicker_coals',
            'led',
            'sound',
            'pdu',
            'led_display',
            'heating_mode',
            'levels_heating',
            'termostat',
            'protection',
            'timer',
            'type_light',
            'Humidification',
            'embedding',
            'country',
            'power_hot',
            'published',
            'template',
            'parser',
            'vendor',
            'color',
            'color_id',
            'size_d',
            'size_h',
            'size_w',
            'old_price',
        ))


def write_csv(data):
    with open(fn.PATH + '/electrokamin4u.csv', 'a', newline='', encoding='utf-8') as f:

        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow((
            data['title'],
            data['parent'],
            data['article'],
            data['alias'],
            data['price'],
            data['content'],
            data['video_link'],
            data['gallery'],
            data['images'],
            data['dop_info'],
            data['type_id'],
            data['size'],
            data['hearth'],
            data['type_hearth'],
            data['drova'],
            data['bright'],
            data['mute_drov'],
            data['flicker_coals'],
            data['led'],
            data['sound'],
            data['pdu'],
            data['led_display'],
            data['heating_mode'],
            data['levels_heating'],
            data['termostat'],
            data['protection'],
            data['timer'],
            data['type_light'],
            data['Humidification'],
            data['embedding'],
            data['country'],
            data['power_hot'],
            data['published'],
            data['template'],
            'electrokamin4u',
            data['vendor'],
            data['color'],
            data['color_id'],
            data['size_d'],
            data['size_h'],
            data['size_w'],
            data['old_price'],
        ))


def change_csv(old, new):
    if fn.path.isfile(new):
        fn.remove(new)
    if fn.path.isfile(old):
        fn.rename(old, new)


def electrokamin4u(url):

    # 18-uglovoi-kaminokomplekt?id_category=18&n=500
    base_site = 'https://electrokamin4u.ru'
    html = fn.get_html(url)

    if html == False:
        return False

    soup = fn.BeautifulSoup(html, 'html.parser')
    list = soup.find('ul', class_='product_list').find_all(
        'li', class_='ajax_block_product')
    if not list:
        return False
    i = 0

    for li in list:
        i += 1
        data = {}
        cart_url = li.find('a').get('href')
        cart_html = fn.get_html(cart_url)

        if cart_html == False:
            return False
        cart = fn.BeautifulSoup(cart_html, 'html.parser')

        # категории
        if 'navesnoy-kaminokomplekt' in cart_url:
            parent = 1718
        elif 'klassicheskie' in cart_url:
            parent = 2259
        elif 'nastennye' in cart_url:
            parent = 1718
        elif 'vstraivaemye' in cart_url:
            parent = 371
        elif 'iz-kamnya' in cart_url:
            parent = 309
        elif 'iz-dereva' in cart_url:
            parent = 306
        elif 'sovremennye' in cart_url:
            parent = 2260
        elif 'multimedijnye-hi-tech' in cart_url:
            parent = 305
        elif 'napolnye' in cart_url:
            parent = 1717
        elif 'temnye-i-venge' in cart_url:
            parent = 363
        elif 'bezhevye-i-belye' in cart_url:
            parent = 362
        elif 'malenkie' in cart_url:
            parent = 308
        elif 'bolshie-i-shirokie' in cart_url:
            parent = 2259
        elif 'uzkie' in cart_url:
            parent = 2259
        elif 'infrakrasnyj-obogrev' in cart_url:
            parent = 2259
        elif 'bez-obogreva' in cart_url:
            parent = 2259
        elif 'pristennye' in cart_url:
            parent = 361
        elif 'uglovoi-kaminokomplekt' in cart_url:
            parent = 311
        elif '3d-ochagi' in cart_url:
            parent = 396
        elif '3d-elektrokaminy' in cart_url:
            parent = 310
        elif '3d-kaminokomplekty' in cart_url:
            parent = 310
        elif 'standartnyi-ochag' in cart_url:
            parent = 369
        elif 'elektroochagi' in cart_url:
            parent = 369
        elif 'shirokiy-ochag' in cart_url:
            parent = 495
        elif 'pechi' in cart_url:
            parent = 1474
        else:
            parent = 26  # пусть будет по умолчанию

        try:
            data['title'] = cart.find('title').text.replace(u'\xa0', ' ').strip()
        except:
            data['title'] = ''
        try:
            data['parent'] = parent
        except:
            data['parent'] = ''
        try:
            data['article'] = cart.find(
                'p', {"id": "product_reference"}).find('span').text
        except:
            data['article'] = ''
        try:
            data['alias'] = 'RLF-' + \
                fn.translate(data['title']) + \
                fn.translate(data['article']).replace('--', '-')
        except:
            data['alias'] = ''
        try:
            data['price'] = cart.find('span', {"id": "our_price_display"}).text.replace(
                ' ', '').replace('e', '')
        except:
            data['price'] = ''
        try:
            data['old_price'] = cart.find(
                'span', {"id": "old_price_display"}).text.replace(' ', '').replace('e', '')
        except:
            data['old_price'] = 0
        try:
            data['content'] = cart.find(
                'div', {"id": "short_description_content"})
        except:
            data['content'] = ''
        try:
            data['video_link'] = cart.find('div', {"id": "prod_video"}).find('iframe').get('src').replace(
                'https://www.youtube.com/embed/', '').replace('http://www.youtube.com/embed/', '')
        except:
            data['video_link'] = ''
        try:
            images = cart.find('ul', {"id": "thumbs_list_frame"}).find_all('a')
        except:
            images = ''

        data['gallery'] = ''
        data['images'] = ''
        im = []
        if images:
            for image in images:
                im.append(image.get('href'))

        data['gallery'] = '|'.join(im)
        data['images'] = im[0]

        if not data['images'] and not data['gallery']:
            continue

        features = cart.find('table', {"id": "quan"})
        data['dop_info'] = features

        data['type_id'] = ''
        data['size'] = ''
        data['hearth'] = ''
        data['type_hearth'] = ''
        data['drova'] = ''
        data['bright'] = ''
        data['mute_drov'] = ''
        data['flicker_coals'] = ''
        data['led'] = ''
        data['sound'] = ''
        data['pdu'] = ''
        data['led_display'] = ''
        data['heating_mode'] = ''
        data['levels_heating'] = ''
        data['termostat'] = ''
        data['protection'] = ''
        data['timer'] = ''
        data['type_light'] = ''
        data['Humidification'] = ''
        data['embedding'] = ''
        data['country'] = ''
        data['power_hot'] = ''
        data['color'] = ''
        data['size_d'] = ''
        data['size_h'] = ''
        data['size_w'] = ''

        try:
            for tr in features.find_all('tr'):
                tds = tr.find_all('td')
                if tds:
                    lv = tds[1].text
                    tn = tds[0].text
                    if 'Тип установки' in tn:
                        data['type_id'] = lv if lv else ''
                    elif 'Габаритные размеры электроочага' in tn:
                        continue
                    elif 'Встраиваемые размеры электроочага' in tn:
                        continue
                    elif 'Габаритные размеры электрокамина' in tn:
                        data['size'] = lv.replace(' ', '').replace('см', '').replace(
                            'покатету', '').replace(',', '.') if lv else ''
                    elif 'абаритные размеры' in tn:
                        data['size'] = lv.replace(' ', '').replace('см', '').replace(
                            'покатету', '').replace(',', '.') if lv else ''
                    elif 'Используемый в комплекте электроочаг' in tn:
                        data['hearth'] = lv if lv else ''
                    elif 'Цветовые тона обрамления' in tn:
                        data['color'] = lv if lv else 'Не задан'
                    elif 'Тип материалов' in tn:
                        data['material'] = lv if lv else ''
                    elif 'Тип очага' in tn:
                        data['type_hearth'] = lv if lv else ''
                    elif 'Технология имитации пламени' in tn:
                        data['drova'] = lv if lv else ''
                    elif 'Регулировка яркости пламени' in tn:
                        data['bright'] = 1 if lv != 'Нет' else 0
                    elif 'Эффект затухания дров при выключении' in tn:
                        data['mute_drov'] = 1 if lv != 'Нет' else 0
                    elif 'Эффект мерцания углей' in tn:
                        data['flicker_coals'] = 1 if lv != 'Нет' else 0
                    elif 'Дополнительная подсветка' in tn:
                        data['led'] = 1 if lv != 'Нет' else 0
                    elif 'потрескивания дров в очаге' in tn:
                        data['sound'] = 1 if lv != 'Нет' else 0
                    elif 'Пульт дистанционного управления' in tn:
                        data['pdu'] = 1 if lv != 'Нет' else 0
                    elif 'LED дисплей' in tn:
                        data['led_display'] = 1 if lv != 'Нет' else 0
                    elif 'Тип обогрева' in tn:
                        data['heating_mode'] = lv if lv else ''
                    elif 'Количество уровней регулировки обогрева' in tn:
                        data['levels_heating'] = lv if lv else ''
                    elif 'Термостат' in tn:
                        data['termostat'] = 1 if lv != 'Нет' else 0
                    elif 'Защита от перегрева' in tn:
                        data['protection'] = 1 if lv != 'Нет' else 0
                    elif 'Таймер отключения' in tn:
                        data['timer'] = 1 if lv != 'Нет' else 0
                    elif 'Тип ламп' in tn:
                        data['type_light'] = lv if lv else ''
                    elif 'Увлажнение воздуха' in tn:
                        data['Humidification'] = 1 if lv != 'Нет' else 0
                    elif 'Возможность встраивания в стену' in tn:
                        data['embedding'] = lv if lv else ''
                    elif 'Страна' in tn:
                        data['country'] = lv if lv else ''
                    elif 'Мощность обогрева' in tn:
                        data['power_hot'] = lv if lv else ''
        except:
            pass

        try:
            list_size = data['size'].split('x')
            data['size_h'] = int(float(list_size[0])) * 10
            data['size_w'] = int(float(list_size[1])) * 10
            data['size_d'] = int(float(list_size[2])) * 10
        except:
            pass

        # print(cart_url)
        # print(list_size)
        # print('{}x{}x{}'.format(data['size_h'], data['size_w'], data['size_d']))

        if not data['old_price']:
            data['old_price'] = 0
        data['published'] = 1
        data['template'] = 7
        data['vendor'] = 4
        data['color_id'] = data['color']

        # добавляем в csv файл
        write_csv(data)
    try:
        next = soup.find("li", {"id": "pagination_next_bottom"}).find('a')
        if next:
            electrokamin4u(base_site + next.get('href'))
    except:
        pass
