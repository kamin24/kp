import fun as fn


def create_csv():
    with open(fn.PATH + '/caminshop.csv', 'w', newline='', encoding='utf-8') as f:
        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow((
            'title',
            'parent',
            'article',
            'alias',
            'price',
            'old_price',
            'content',
            'video',
            'gallery',
            'images',
            'material',
            'size_w',
            'size',
            'hearth',
            'color',
            'color_id',
            'country',
            'weight',
            'ndecor',
            'heating_mode',
            'sound',
            'pdu',
            'published',
            'template',
            'parser',
            'vendor',
            'size_d',
            'size_h'
        ))


def write_csv(data):
    with open(fn.PATH + '/caminshop.csv', 'a', newline='', encoding='utf-8') as f:

        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow((
            data['title'],
            data['parent'],
            data['article'],
            data['alias'],
            data['price'],
            data['old_price'],
            data['content'],
            data['video'],
            data['gallery'],
            data['images'],
            data['material'],
            data['size_w'],
            data['size'],
            data['hearth'],
            data['color'],
            data['color_id'],
            data['country'],
            data['weight'],
            data['ndecor'],
            data['heating_mode'],
            data['sound'],
            data['pdu'],
            1,
            7,
            'caminshop',
            data['vendor'],
            data['size_d'],
            data['size_h']
        ))


def change_csv(old, new):
    if fn.path.isfile(new):
        fn.remove(new)
    if fn.path.isfile(old):
        fn.rename(old, new)


def run(url):

    html = fn.get_html(url)
    xml = fn.BeautifulSoup(html, 'xml')

    list_loc = xml.findAll('loc')
    list_url = []
    data = {}

    for loc in list_loc:
        url = loc.text
        if('product' in url and url.count('/') == 4):
            list_url.append(url)
	
    list_c = []
    for product_url in list_url:
        product_html = fn.get_html(product_url)
        # product_html = fn.get_html('https://caminshop.ru/products/interflame-munhen-c-ochagom-triumph-33-3d')

        if product_html == False:
            continue

        soup = fn.BeautifulSoup(product_html, 'html.parser')

        try:
            data['title'] = soup.find(
                'div', class_='product__name').text.strip()
        except:
            data['title'] = 'Камин без названия'

        category = soup.findAll(
            'div', class_='bread-crumbs__item bread-crumbs__text')[-1].text.strip()

        data['parent'] = 26
		
        # категории
        if '3D Электрокамины из МДФ' in category:
            data['parent'] = 310
        elif 'led электрокамины из мдф' in category:
            data['parent'] = 2259
        elif 'LED электрокамины из камня' in category:
            data['parent'] = 309
        elif '3D Электрокамины из камня' in category:
            data['parent'] = 309
        elif 'Электрокамины под tv' in category:
            data['parent'] = 305
        elif '3d электроочаги' in category:
            data['parent'] = 396
        elif '2D Электроочаги' in category:
            data['parent'] = 369

        # фото
        images = soup.find(
            'div', class_='product-media-slider').find_all('a', class_='fancy-img')
        list_images = []
        if images:
            for image in images:
                list_images.append('https:' + image.get('href'))

        data['gallery'] = '|'.join(list_images)
        data['images'] = list_images[0]

        # видео
        videos = soup.find(
            'div', class_='product-media-slider').find_all('a', class_='video-link')
        list_videos = []
        if videos:
            for video in videos:
                list_videos.append(video.find('img').get('src').replace(
                    '//img.youtube.com/vi/', '').replace('/0.jpg', ''))  # ['aZOO_4yUv2k', 'AxMX9LcXBq8']

        # функционал на сайте заточен только под одно видео.
        if list_videos:
            data['video'] = list_videos[0]

        

        

        try:
            data['article'] = soup.find(
                'div', class_='product__serial').text.replace('Артикул: ', '').strip()
        except:
            data['article'] = 'CMS-' + \
                str(fn.hashlib.md5(data['title'].encode(
                    'utf-8')).hexdigest()[:6])

        productID = soup.find(
                'a', {"href":"#one-click-form"}).get('data-product-id')
        
        data['article'] = "{}-{}".format(data['article'], productID)

        try:
            data['alias'] = 'CMS-' + fn.translate(data['title']) + fn.translate(
                data['article']).replace('--', '-').replace('/', '-')
        except:
            data['alias'] = ''

        list_price = soup.findAll('span', class_='product-price-data')

        try:
            data['price'] = list_price[0].text
        except:
            data['price'] = 0

        data['old_price'] = 0 if len(list_price) < 2 else list_price[1].text

        try:
            desc = soup.find('div', class_='product__desc')
        except:
            desc = ''

        try:
            desc_full = soup.find('div', {"id": "product-full-desc"}).replace(
                '<a href="tel:+74994996770">+7 (499) 499-67-70</a>', '')
        except:
            desc_full = ''

        data['content'] = str(desc) + str(desc_full)

        try:
            attributes = soup.find('table', class_='product-attributes')
        except:
            attributes = ''

        if attributes:

            data['material'] = ''
            data['size_w'] = ''
            data['color'] = ''
            data['hearth'] = ''

            for tr in attributes.find_all('tr'):
                tds = tr.find_all('td')
                if tds:
                    # ШИРИНА КАМИНА, Ширина, ЦВЕТ, ВЫБЕРИТЕ ОЧАГ, ВЫБЕРИТЕ КАМЕНЬ
                    lv = tds[0].text
                    tn = tds[1].text.strip()  # value

                    if 'ШИРИНА КАМИНА' in lv or 'Ширина' in lv:
                        data['size_w'] = tn.replace(' см', '0') if tn else ''  # 140 см -> 1400
                    elif 'ЦВЕТ' in lv:
                        data['color'] = tn if tn else ''
                    elif 'ВЫБЕРИТЕ ОЧАГ' in lv:
                        data['hearth'] = tn if tn else ''
                    elif 'ВЫБЕРИТЕ КАМЕНЬ' in lv:
                        data['material'] = tn if tn else ''


        desc_table = soup.find('div', class_='product__desc').findAll('table')

        data['color_id'] = ''
        data['country'] = ''
        data['size'] = ''
        data['weight'] = ''
        data['ndecor'] = ''
        data['heating_mode'] = ''
        data['sound'] = ''
        data['pdu'] = ''
        data['size_d'] = ''
        data['size_h'] = ''

        for table in desc_table:
            caption = table.find('caption')

            if caption:
                data['color_id'] = []
                for tr in table.find_all('tr'):
                    tds = tr.find_all('td')
                    if tds:
                        lv = tds[0].text.strip().lower()
                        data['color_id'].append(lv)
                data['color_id'] = '|'.join(data['color_id'])
                    
            else:
                for tr in table.find_all('tr'):
                    tds = tr.find_all('td')
                    if tds:
                        lv = tds[0].text.strip()
                        tn = tds[1].text.strip()  # value

                        if 'Габаритные Размеры' in lv:
                            data['size'] = tn if tn else ''

                            try:
                                list_size = tn.split('х')
                                data['size_w'] = list_size[0]
                                data['size_h'] = list_size[1]
                                data['size_d'] = list_size[2]
                            except:
                                pass
                                
                        elif 'Материал Дерева' in lv and data['material'] == '':
                            data['material'] = tn if tn else ''
                        elif 'Производство Очага' in lv:
                            data['country'] = tn if tn else ''
                        elif 'Подходящие Очаги' in lv and data['hearth'] == '':
                            data['hearth'] = tn if tn else ''
                        elif 'Вес Брутто' in lv:
                            data['weight'] = tn.replace(' кг ', '').strip() if tn else ''
                        elif 'Декоративный Режим' in lv:
                            data['ndecor'] = tn if tn else ''
                        elif 'Элемент Обогрева' in lv:
                            data['heating_mode'] = tn if tn else ''
                        elif 'Звук' in lv:
                            data['sound'] = 1 if tn == 'есть' else ''
                        elif 'Пульт ДУ' in lv:
                            data['pdu'] = 1 if tn == 'есть' else ''

        data['vendor'] = 992
        
        write_csv(data)
        # print(data)

        # break