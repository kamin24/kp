import csv  # Импорт библиотеки для работы с csv
import io  # Импорт библиотек для получения xml
import xml.etree.ElementTree as ET  # Импорт библиотеки для xml
import zipfile

import requests
from bs4 import BeautifulSoup  # Импорт библиотеки для парсинга запроса

import fun as fn

# Из библиотек надо установить requests и bs 4 ( pip install bs4 и pip install requests )


def get_zip(data):  # Функция получения zip
    req = requests.post(
        'https://dantexgroup.ru/export/yml/export.php', data=data)
    soup = BeautifulSoup(req.text, 'html.parser')
    zip_link = 'https://dantexgroup.ru' + soup.a.get('href')
    r = requests.get(zip_link, stream=True)
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extractall()


def get_pic_string(iter):  # Функция упрощения получения ссылок на фотки
    answ = ''
    for pic in iter:
        answ = answ + pic.text + '|'
    return answ[0:-1]


def file_parse():  # Функция парсинга xml
    origin_item_data = {
        'parser': '',
        'published': '',
        'template': '',
        'alias': '',
        'parent': '',
        'model': '',
        'vendor': '',
        'country_of_origin': '',
        'price': '',
        'image': '',
        'pictures': '',
        'Тип очага': '',
        'Технология пламени': '',
        'Установочный размер (В×Ш×Г)': '',
        '3D': '',
        'Мощность обогрева': '',
        'Пульт Д/У': '',
        'Опции': '',
        'Цвет очага': '',
        'Гарантия': '',
        'Габаритный размер': '',
        'Вес нетто': '',
        'size_d': '',
        'size_h': '',
        'size_w': '',
        'pdu': '',
        'color_id': '',
        'article': '',
        'description': '',
    }
    translate_table = {'Тип очага': 'type', 'Технология пламени': 'Flame_technology', 'Установочный размер (В×Ш×Г)': 'size_install',
                       '3D': '3D', 'Мощность обогрева': 'power_hot', 'Пульт Д/У': 'Remote controller', 'Опции': 'options', 'Цвет очага': 'color',
                       'Гарантия': 'Warranty', 'Габаритный размер': 'size', 'Вес нетто': 'weight'}
    with open('dantex.csv', mode='w') as file:
        file_writer = csv.writer(file, delimiter=';')
        title_string_array = []
        for i in origin_item_data.keys():
            if i in translate_table.keys():
                title_string_array.append(translate_table[i])
            else:
                title_string_array.append(i)
        file_writer.writerow(title_string_array)
        file.close()
    root = ET.parse('yml_catalog.xml').getroot()

    VENDORS = {
        "Dimplex": 1003,
        "Royal Flame": 2,
    }
    CATEGORIES = {
        "238": 369,
        "242": 5144,
        "1247": 370,
        "239": 369,
        "1306": 371,
        "1248": 495,
        "240": 495,
        "1237": 371,  # встр
        "1236": 370,  # настен
        "1235": 369,
    }

    for offer in root.iter('offer'):
        item_data = origin_item_data

        if not offer.get('id'):
            continue

        if offer.find('categoryId') is None and 'каминокомплект' not in offer.find('model').text.lower():
            continue

        if 'портал' in offer.find('model').text.lower():
            continue

        item_data['article'] = f"DTX-{offer.get('id')}"
        try:
            item_data['model'] = offer.find('model').text
        except:
            item_data['model'] = ''
        try:
            item_data['parent'] = CATEGORIES.get(
                offer.find('categoryId').text, 26)
        except:
            item_data['parent'] = 26
        try:
            item_data['vendor'] = VENDORS.get(offer.find('vendor').text, '')
        except:
            item_data['vendor'] = ''
        try:
            item_data['country_of_origin'] = offer.find(
                'country_of_origin').text
        except:
            item_data['country_of_origin'] = ''
        try:
            item_data['price'] = float(offer.find('price').text)
        except:
            item_data['price'] = 0
        try:
            item_data['description'] = offer.find('description').text
        except:
            item_data['description'] = ''
        for param in offer.iter('param'):
            try:
                if param.attrib['name'] in item_data.keys():
                    item_data[param.attrib['name']] = param.text
            except:
                pass
        try:
            item_data['pictures'] = get_pic_string(offer.iter('picture'))
        except:
            item_data['pictures'] = ''
        try:
            item_data['image'] = item_data['pictures'].split('|')[0]
        except:
            item_data['image'] = ''

        if item_data['Цвет очага']:
            item_data['color_id'] = item_data['Цвет очага']
        if item_data['Пульт Д/У']:
            item_data['pdu'] = 1
        if item_data['Габаритный размер']:
            sizes = item_data['Габаритный размер'].split('x')
            if sizes:
                item_data['size_h'] = int(sizes[0])
                item_data['size_w'] = int(sizes[1])
                item_data['size_d'] = int(sizes[2].split(' ')[0])
        
        item_data['parser'] = 'dantex'
        item_data['published'] = 0
        item_data['template'] = 7
        item_data['alias'] = f"{fn.translate(item_data['model'])}_{item_data['article']}"

        save_csv_raw(item_data)


def save_csv_raw(offer):  # Функция сохранения csv
    with open('dantex.csv', mode='a') as file:
        file_writer = csv.writer(file, delimiter=';')
        file_writer.writerow(offer.values())
        file.close()


def form_data_parse():  # Функция формирования data для запроса
    data = {}
    raw_data = '''options[0][name]: pic-link
options[0][value]: Y
sections[0][name]: 1229
sections[0][value]: 1229
sections[1][name]: 1234
sections[1][value]: 1234
sections[2][name]: 1248
sections[2][value]: 1248
sections[3][name]: 1238
sections[3][value]: 1238
sections[4][name]: 1233
sections[4][value]: 1233
sections[5][name]: 1306
sections[5][value]: 1306
sections[6][name]: 1237
sections[6][value]: 1237
sections[7][name]: 1232
sections[7][value]: 1232
sections[8][name]: 1247
sections[8][value]: 1247
sections[9][name]: 1236
sections[9][value]: 1236
sections[10][name]: 1231
sections[10][value]: 1231
sections[11][name]: 1235
sections[11][value]: 1235
sections[12][name]: 126
sections[12][value]: 126
sections[13][name]: 241
sections[13][value]: 241
sections[14][name]: 240
sections[14][value]: 240
sections[15][name]: 125
sections[15][value]: 125
sections[16][name]: 239
sections[16][value]: 239
sections[17][name]: 238
sections[17][value]: 238
sections[18][name]: 124
sections[18][value]: 124
sections[19][name]: 242
sections[19][value]: 242
sections[20][name]: 179
sections[20][value]: 179
sections[21][name]: 1246
sections[21][value]: 1246
sections[22][name]: 1264
sections[22][value]: 1264
sections[23][name]: 1263
sections[23][value]: 1263
sections[24][name]: 1245
sections[24][value]: 1245
sections[25][name]: 1262
sections[25][value]: 1262
sections[26][name]: 1261
sections[26][value]: 1261
sections[27][name]: 1244
sections[27][value]: 1244
sections[28][name]: 1260
sections[28][value]: 1260
sections[29][name]: 1259
sections[29][value]: 1259
sections[30][name]: 1243
sections[30][value]: 1243
sections[31][name]: 1258
sections[31][value]: 1258
sections[32][name]: 1257
sections[32][value]: 1257'''
    for i in raw_data.split('\n'):
        g = i.split(': ')
        data[g[0]] = g[1]
    return(data)


def run():
    get_zip(form_data_parse())
    file_parse()
