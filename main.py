import fun as fn
import rusklimat
import tdbg
import interflame
import electrokamin4u
import caminshop
import belfortkamin
import mosng
import pech
import dantex


def belfortkamin_parser():
    category_belfortkamin = [
        'https://belfortkamin.ru/category/fire-chambers/',
        'https://belfortkamin.ru/category/gazovye-topki/',
        'https://belfortkamin.ru/category/pechi-kaminy-kaminokomlekty/',
        'https://belfortkamin.ru/category/biokaminy/',
        'https://belfortkamin.ru/category/oblicovki-dlja-topok/',
        'https://belfortkamin.ru/category/kaminnye-reshetki/',
        'https://belfortkamin.ru/category/opcii_dlya_kaminov/',
        'https://belfortkamin.ru/category/discount/',
    ]

    # переименовываем старый файл
    if fn.path.isfile(fn.PATH + '/belfortkamin.csv'):
        belfortkamin.change_csv(
            fn.PATH + '/belfortkamin.csv', fn.PATH + '/belfortkamin_prev.csv')
    # создаем новый файл csv
    belfortkamin.create_csv()
    for url in category_belfortkamin:
        belfortkamin.run(url)


def electrokamin4u_parser():
    category_electrokamin4u = [
        'https://electrokamin4u.ru/13-elektrokaminy',
        'https://electrokamin4u.ru/418-elektroochagi?id_category=418&n=815',
    ]

    # переименовываем старый файл
    if fn.path.isfile(fn.PATH + '/electrokamin4u.csv'):
        electrokamin4u.change_csv(
            fn.PATH + '/electrokamin4u.csv', fn.PATH + '/electrokamin4u_prev.csv')
    # создаем новый файл csv
    electrokamin4u.create_csv()
    for url in category_electrokamin4u:
        electrokamin4u.electrokamin4u(url)


def interflame_parser():
    category_interflame = [
        'https://www.interflame.ru/catalog/kaminy-classic-flame/',
        'https://www.interflame.ru/catalog/ochagi/',
        'https://www.interflame.ru/catalog/portaly-s-kamnem/',
        'https://www.interflame.ru/catalog/reznye-portaly/'
    ]
    # переименовываем старый файл
    if fn.path.isfile(fn.PATH + '/interflame.csv'):
        interflame.change_csv(fn.PATH + '/interflame.csv',
                              fn.PATH + '/interflame_prev.csv')
    # создаем новый файл csv
    interflame.create_csv()
    for url in category_interflame:
        interflame.interflame(url)


def backup_csv(donor):
    # переименовываем старый файл
    if fn.path.isfile(fn.PATH + '/{}.csv'.format(donor)):
        pech.change_csv(fn.PATH + '/{}.csv'.format(donor),
                        fn.PATH + '/{}_prev.csv'.format(donor))


def main():

    # if fn.path.isfile(fn.PATH + '/mosng.csv'):
    #     mosng.change_csv(fn.PATH + '/mosng.csv', fn.PATH + '/mosng_prev.csv')
    # mosng.create_csv()
    # mosng.run()

    # dantex
    print('парсим dantex')
    dantex.run()

    # pech
    print('парсим pech')
    backup_csv('pech')
    pech.run()

    # belfortkamin
    # print('парсим belfortkamin')
    # belfortkamin_parser()

    # electrokamin4u
    print('парсим electrokamin4u')
    electrokamin4u_parser()

    # caminshop
    # print('парсим caminshop')
    # переименовываем старый файл
    # if fn.path.isfile(fn.PATH + '/caminshop.csv'):
    # caminshop.change_csv(fn.PATH + '/caminshop.csv', fn.PATH + '/caminshop_prev.csv')
    # caminshop.create_csv()
    # caminshop.run('https://caminshop.ru/sitemap.xml')

    # rusklimat
    # print('парсим rusklimat')
    # rusklimat.rusklimat()

    # tdbg
    # print('парсим tdbg')
    # tdbg.tdbg()

    # interflame
    # print('парсим interflame')
    # interflame_parser()

    print('парсить закончили')

    # mail.mailto('Уведомление парсера об УДАЛЕНИИ товаров на сайтах донорах', 'Эти товары были удалены или просто не найдены скриптом', '<p><a href="#">Камин №1</a></p>')
    # mail.mailto('Уведомление парсера о ДОБАВЛЕНИИ товаров на сайтах донорах', 'Эти товары были на', '<p><a href="#">Камин №1</a></p>')


if __name__ == '__main__':
    main()
