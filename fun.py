import requests
from bs4 import BeautifulSoup
import csv
import re
import hashlib
import time
from os import rename, path, remove

PATH = path.dirname(path.abspath(__file__))


def get_html(url, header=None):
    # time.sleep(2)
    header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0',
        'Pragma': 'no-cache',
        'Referer': 'https://www.mosng.ru/',
        'Connection': 'keep-alive',
        'Cache-Control': 'no-cache',
        'Accept-Encoding': 'gzip, deflate, br',
        'Cookie': 'jv_visits_count_HtQZFO5F0G=5; viewedProduct=%7B%229824%22%3A%7B%22ID%22%3A%229824%22%2C%22NAME%22%3A%22%5Cu041a%5Cu0430%5Cu043c%5Cu0438%5Cu043d%5Cu043e%5Cu043a%5Cu043e%5Cu043c%5Cu043f%5Cu043b%5Cu0435%5Cu043a%5Cu0442+Lindos+%5Cu0431%5Cu0435%5Cu043b%5Cu044b%5Cu0439+%5Cu0441+%5Cu043e%5Cu0447%5Cu0430%5Cu0433%5Cu043e%5Cu043c+Symphony+DF2624-L%22%2C%22PICTURE%22%3A%22%5C%2Fupload%5C%2Fresize_cache%5C%2Fiblock%5C%2Fac7%5C%2F280_280_12929a8bac62cdd4017a71388beeea020%5C%2Fcombo_2573_2184.jpg%22%2C%22URL%22%3A%22%5C%2Fcatalog%5C%2Fkaminokomplekt-lindos-belyy-s-ochagom-symphony-26%5C%2F%22%2C%22TIME%22%3A1581610411%7D%2C%2210288%22%3A%7B%22ID%22%3A%2210288%22%2C%22NAME%22%3A%22%5Cu041f%5Cu043e%5Cu0440%5Cu0442%5Cu0430%5Cu043b+Lindos+Graphite+Grey+%5Cu043f%5Cu043e%5Cu0434+%5Cu043e%5Cu0447%5Cu0430%5Cu0433+Vision+60%22%2C%22PICTURE%22%3A%22%5C%2Fupload%5C%2Fresize_cache%5C%2Fiblock%5C%2F332%5C%2F280_280_12929a8bac62cdd4017a71388beeea020%5C%2FLindos_Graphite_Grey_pod_Vision_60_s_ochagom.jpg%22%2C%22URL%22%3A%22%5C%2Fcatalog%5C%2Fportal-lindos-graphite-grey-pod-ochag-vision-60%5C%2F%22%2C%22TIME%22%3A1581966904%7D%2C%223866%22%3A%7B%22ID%22%3A%223866%22%2C%22NAME%22%3A%22%5Cu041d%5Cu0430%5Cu0441%5Cu0442%5Cu0435%5Cu043d%5Cu043d%5Cu044b%5Cu0439+%5Cu043a%5Cu0430%5Cu043c%5Cu0438%5Cu043d+Design+885CG%22%2C%22PICTURE%22%3A%22%5C%2Fupload%5C%2Fresize_cache%5C%2Fiblock%5C%2Ffc2%5C%2F280_280_12929a8bac62cdd4017a71388beeea020%5C%2Fdesigne_885cg_ochag.jpg%22%2C%22URL%22%3A%22%5C%2Fcatalog%5C%2Froyal-flame-designe-885cg%5C%2F%22%2C%22TIME%22%3A1581967831%7D%2C%2210279%22%3A%7B%22ID%22%3A%2210279%22%2C%22NAME%22%3A%22%5Cu041a%5Cu0430%5Cu043c%5Cu0438%5Cu043d%5Cu043e%5Cu043a%5Cu043e%5Cu043c%5Cu043f%5Cu043b%5Cu0435%5Cu043a%5Cu0442+Lyon+60+%5Cu0441+%5Cu043e%5Cu0447%5Cu0430%5Cu0433%5Cu043e%5Cu043c+Vision+60+LOG+LED%22%2C%22PICTURE%22%3A%22%5C%2Fupload%5C%2Fresize_cache%5C%2Fiblock%5C%2F60f%5C%2F280_280_12929a8bac62cdd4017a71388beeea020%5C%2Flyon_1024.jpg%22%2C%22URL%22%3A%22%5C%2Fcatalog%5C%2Fkaminokomplekt-lyon-60-s-ochagom-vision-60-log-led%5C%2F%22%2C%22TIME%22%3A1581968596%7D%2C%2210277%22%3A%7B%22ID%22%3A%2210277%22%2C%22NAME%22%3A%22%5Cu041a%5Cu0430%5Cu043c%5Cu0438%5Cu043d%5Cu043e%5Cu043a%5Cu043e%5Cu043c%5Cu043f%5Cu043b%5Cu0435%5Cu043a%5Cu0442+Valletta+60+%5Cu0441+%5Cu043e%5Cu0447%5Cu0430%5Cu0433%5Cu043e%5Cu043c+Vision+60+LOG+LED%22%2C%22PICTURE%22%3A%22%5C%2Fupload%5C%2Fresize_cache%5C%2Fiblock%5C%2F51e%5C%2F280_280_12929a8bac62cdd4017a71388beeea020%5C%2FValletta_Vision_60_log_1024.jpg%22%2C%22URL%22%3A%22%5C%2Fcatalog%5C%2Fkaminokomplekt-valletta-60-s-ochagom-vision-60-log%5C%2F%22%2C%22TIME%22%3A1581968603%7D%2C%227537%22%3A%7B%22ID%22%3A%227537%22%2C%22NAME%22%3A%22%5Cu041a%5Cu0430%5Cu043c%5Cu0438%5Cu043d%5Cu043e%5Cu043a%5Cu043e%5Cu043c%5Cu043f%5Cu043b%5Cu0435%5Cu043a%5Cu0442+Athena+%5Cu0442%5Cu0435%5Cu043c%5Cu043d%5Cu044b%5Cu0439+%5Cu0434%5Cu0443%5Cu0431+c+%5Cu043e%5Cu0447%5Cu0430%5Cu0433%5Cu043e%5Cu043c+Gannon+2011%22%2C%22PICTURE%22%3A%22%5C%2Fupload%5C%2Fresize_cache%5C%2Fiblock%5C%2F173%5C%2F280_280_12929a8bac62cdd4017a71388beeea020%5C%2Fathena4.jpg%22%2C%22URL%22%3A%22%5C%2Fcatalog%5C%2Fkaminokomplekt-athena-temnyy-dub-c-ochagom-gannon%5C%2F%22%2C%22TIME%22%3A1582054952%7D%2C%2210282%22%3A%7B%22ID%22%3A%2210282%22%2C%22NAME%22%3A%22%5Cu041a%5Cu0430%5Cu043c%5Cu0438%5Cu043d%5Cu043e%5Cu043a%5Cu043e%5Cu043c%5Cu043f%5Cu043b%5Cu0435%5Cu043a%5Cu0442+Denver+60+%5Cu0441+%5Cu043e%5Cu0447%5Cu0430%5Cu0433%5Cu043e%5Cu043c+Vision+60+LOG+LED%22%2C%22PICTURE%22%3A%22%5C%2Fupload%5C%2Fresize_cache%5C%2Fiblock%5C%2Ffa0%5C%2F280_280_12929a8bac62cdd4017a71388beeea020%5C%2FShateau_vision60_log_1024.jpg%22%2C%22URL%22%3A%22%5C%2Fcatalog%5C%2Fkaminokomplekt-denver-60-s-ochagom-vision-60-log-l%5C%2F%22%2C%22TIME%22%3A1582057835%7D%7D; BITRIX_SM_SALE_UID=101422; PHPSESSID=3spg7n8ah8nbrpart65tp83p23; _ct_ids=32c27004%3A4098%3A1493733195; cted=; _ct_session_id=1493733195; _ct_site_id=4098; _ct=100000002721910448; call_s=%3C!%3E%7B%2232c27004%22%3A%5B1582144489%2C1493733195%2C%7B%2284215%22%3A%22273529%22%7D%5D%2C%22d%22%3A2%7D%3C!%3E; jv_enter_ts_HtQZFO5F0G=1582142139009; jv_pages_count_HtQZFO5F0G=3; jv_invitation_time_HtQZFO5F0G=1582142706894; jv_close_time_HtQZFO5F0G=1582142180408'
    } if header is None else header
    try:
        r = requests.get(url, headers=header)
    except requests.exceptions.ConnectionError:
        time.sleep(5)
        print('Повтор')
        return get_html(url, header={})
    if r.ok:  # 200  ## 403 404 504
        return r.text
    return False


def get_vendor(name):
    print('get_vendor: ', name)
    r = requests.get('https://roskaminy.ru/api/vendors.json', params={
        'key': 'KNjnbchbhb4cbediwf',
        'mode': 'get',
        'name': str(name),
    })
    try:
        res = r.json()
    except:
        print('get_vendor повтор: ', name)
        time.sleep(5)
        return get_vendor(name)
    print('get_vendor=', name)
    return res


def get_vendors():
    print("get_vendors")
    r = requests.get('https://roskaminy.ru/api/vendors.json', params={
        'key': 'KNjnbchbhb4cbediwf',
        'mode': 'get',
        'all': 'all',
    })
    try:
        res = r.json()
    except:
        print('get_vendors повтор')
        time.sleep(5)
        return get_vendors()
    return res


def get_resource(id):
    print('get_resource: ', id)
    r = requests.get('https://roskaminy.ru/api/resource.json', params={
        'key': 'KNjnbchbhb4cbediwf',
        'mode': 'get',
        'id': str(id),
    })
    res = r.json()
    if not res['ok']:
        print('get_resource повтор: ', id)
        time.sleep(5)
        return get_resource(id)
    return res


def get_resources():
    print('get_resources')
    r = requests.get('https://roskaminy.ru/api/resource.json', params={
        'key': 'KNjnbchbhb4cbediwf',
        'mode': 'get',
    })
    res = r.json()
    if not res['ok']:
        print('get_resources повтор')
        time.sleep(5)
        return get_resources()
    return res['response']


def translate(name):
    # Заменяем пробелы и преобразуем строку к нижнему регистру
    name = name.replace(' ', '-').replace('/', '').replace('_', '-').replace(',', '-').replace(')', '-').replace('(', '-').lower()
    transtable = (
        # Большие буквы
        (u"Щ", u"Sch"),
        (u"Щ", u"SCH"),
        # two-symbol
        (u"Ё", u"Yo"),
        (u"Ё", u"YO"),
        (u"Ж", u"Zh"),
        (u"Ж", u"ZH"),
        (u"Ц", u"Ts"),
        (u"Ц", u"TS"),
        (u"Ч", u"Ch"),
        (u"Ч", u"CH"),
        (u"Ш", u"Sh"),
        (u"Ш", u"SH"),
        (u"Ы", u"Yi"),
        (u"Ы", u"YI"),
        (u"Ю", u"Yu"),
        (u"Ю", u"YU"),
        (u"Я", u"Ya"),
        (u"Я", u"YA"),
        # one-symbol
        (u"А", u"A"),
        (u"Б", u"B"),
        (u"В", u"V"),
        (u"Г", u"G"),
        (u"Д", u"D"),
        (u"Е", u"E"),
        (u"З", u"Z"),
        (u"И", u"I"),
        (u"Й", u"J"),
        (u"К", u"K"),
        (u"Л", u"L"),
        (u"М", u"M"),
        (u"Н", u"N"),
        (u"О", u"O"),
        (u"П", u"P"),
        (u"Р", u"R"),
        (u"С", u"S"),
        (u"Т", u"T"),
        (u"У", u"U"),
        (u"Ф", u"F"),
        (u"Х", u"H"),
        (u"Э", u"E"),
        (u"Ъ", u"`"),
        (u"Ь", u"'"),
        # Маленькие буквы
        # three-symbols
        (u"щ", u"sch"),
        # two-symbols
        (u"ё", u"yo"),
        (u"ж", u"zh"),
        (u"ц", u"ts"),
        (u"ч", u"ch"),
        (u"ш", u"sh"),
        (u"ы", u"yi"),
        (u"ю", u"yu"),
        (u"я", u"ya"),
        # one-symbol
        (u"а", u"a"),
        (u"б", u"b"),
        (u"в", u"v"),
        (u"г", u"g"),
        (u"д", u"d"),
        (u"е", u"e"),
        (u"з", u"z"),
        (u"и", u"i"),
        (u"й", u"j"),
        (u"к", u"k"),
        (u"л", u"l"),
        (u"м", u"m"),
        (u"н", u"n"),
        (u"о", u"o"),
        (u"п", u"p"),
        (u"р", u"r"),
        (u"с", u"s"),
        (u"т", u"t"),
        (u"у", u"u"),
        (u"ф", u"f"),
        (u"х", u"h"),
        (u"э", u"e"),
    )
    # перебираем символы в таблице и заменяем
    for symb_in, symb_out in transtable:
        name = name.replace(symb_in, symb_out)
    # возвращаем переменную
    return name


def strip_tags(string, allowed_tags=''):
    if allowed_tags != '':
        # Get a list of all allowed tag names.
        allowed_tags_list = re.sub(r'[\\/<> ]+', '', allowed_tags).split(',')
        allowed_pattern = ''
        for s in allowed_tags_list:
            if s == '':
                continue
            # Add all possible patterns for this tag to the regex.
            if allowed_pattern != '':
                allowed_pattern += '|'
            allowed_pattern += '<' + s + ' [^><]*>$|<' + s + '>|'
        # Get all tags included in the string.
        all_tags = re.findall(r'<]+>', string, re.I)
        for tag in all_tags:
            # If not allowed, replace it.
            if not re.match(allowed_pattern, tag, re.I):
                string = string.replace(tag, '')
    else:
        # If no allowed tags, remove all.
        string = re.sub(r'<[^>]*?>', '', string)

    return string
