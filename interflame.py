import fun as fn

def create_csv():
	with open(fn.PATH + '/interflame.csv', 'w', newline='', encoding='utf-8') as f:
		writer = fn.csv.writer(f, delimiter = ';')
		writer.writerow((
			'title',
			'alias',
			'image',
			'gallery',
			'price',
			'video_link',
			'content',
			'ochags',
			'dop_info',
			'parent',
			'article',
			'vendor',
			'published',
			'template',
			'parser',
			'color',
			'color_id',
			'size_d',
			'size_h',
			'size_w',
			))

def write_csv(data):
	with open(fn.PATH + '/interflame.csv', 'a', newline='', encoding='utf-8') as f:

		writer = fn.csv.writer(f, delimiter=';')
		writer.writerow((
			data['title'],
			data['alias'],
			data['image'],
			data['gallery'],
			data['price'],
			data['video_link'],
			data['content'],
			data['ochags'],
			data['dop_info'],
			data['parent'],
			data['article'],
			data['vendor'],
			data['published'],
			data['template'],
			'interflame',
			data['color'],
			data['color_id'],
			data['size_d'],
			data['size_h'],
			data['size_w'],
		))

def change_csv(old, new):
	if fn.path.isfile(new):
		fn.remove(new)
	if fn.path.isfile(old):
		fn.rename(old, new)

def interflame(current_url):
	base_site = 'https://www.interflame.ru'
	html = fn.get_html(current_url)

	soup = fn.BeautifulSoup(html, 'html.parser')
	list = soup.find_all('li', class_='catalog-list-item')
	i = 0
	for li in list:
		i += 1
		data = {}
		url = base_site + li.find('a').get('href')
		cart_html = fn.get_html(url)
		cart = fn.BeautifulSoup(cart_html, 'html.parser')

		# название
		try:
			data['title'] = cart.find('h1', class_='catalog__oneitem-title').text
		except:
			data['title'] = ''
		# название
		try:
			data['alias'] = fn.translate(data['title']).replace('--', '-')
		except:
			data['alias'] = ''
		# изображение
		try:
			data['image'] = str(base_site + cart.find('a', class_='catalog-oneitem__image').get('href'))
		except:
			data['image'] = ''
		# галерея
		try:
			gallery = cart.find('div', class_='catalog-oneitem__modification-imgs').find_all('a')
			gl = []
			for image in gallery: gl.append(str(base_site + image.get('href')))
			data['gallery'] = '|'.join(gl)
		except:
			data['gallery'] = ''
			
		if not data['image'] and not data['gallery']:
			continue
		# цвета
		try:
			colors = cart.find('div', class_='catalog-oneitem__modification-imgs').find_all('div', class_="catalog-oneitem__modification-descr")
			cl = []
			for color in colors:
				cl.append(str(color.find('span').text).strip())
			data['color'] = '|'.join(cl)
		except:
			data['color'] = ''
		# цена
		try:
			data['price'] = cart.find('div', class_ = 'catalog-oneitem__price').text.replace(' руб. ', '')
		except:
			data['price'] = 0
		# видео
		try:
			data['video_link'] = cart.find('div', class_="catalog-oneitem__info-block").find('div', class_="content-block").find('iframe').get('src').replace('https://www.youtube.com/embed/','').replace('http://www.youtube.com/embed/','').replace('//www.youtube.com/embed/','')
		except:
			data['video_link'] = ''
		# контент
		try:
			data['content'] = fn.strip_tags(str(cart.find('div', class_="catalog-oneitem__info-block").find('div', class_="content-block")), '<p><b><strong><h2>')
		except:
			data['content'] = ''

		# очаги
		try:
			check_ochag = ''
			check_ochag = cart.find('div', class_="catalog-oneitem__price-text").text # без очага
		except:
			check_ochag = ''
		
		try:
			check_list = cart.find('div', class_="catalog-oneitem__info-block").find_all('li', class_="check-list__item")
			data['ochags'] = ''
			ochags = []
			for idx, check in enumerate(check_list):
				if idx == 0: continue # пропускаем "без очага" 
				ochags.append(check.find('a', class_="check-list__item-name").text)
			data['ochags'] = ' % '.join(ochags)
		except:
			data['ochags'] = ''
		
		data['size'] = ''
		
		try:
			features = cart.find_all('div', class_="catalog-oneitem__property-label")
			data['dop_info'] = ''
			for f in features:
				if 'Размеры' in str(f.text):
					data['size'] = f.find('span').text.replace('(ш)', '').replace('(в)', '').replace('(г)', '').replace(' ', '')
				data['dop_info'] += str('<p>'+f.text+'</p>')
		except:
			data['dop_info'] = ''

		attributes = cart.find('table', class_="product-attributes")		

		if not data['size']:
			try:
				for tr in attributes.find_all('tr'):
					tds = tr.find_all('td')
					if tds:
						lv = tds[1].text
						tn = tds[0].text
				
						if 'Общие Габариты, мм (Ш/В/Г)' in tn: data['size'] = lv.replace('(Ш)', '').replace('(В)', '').replace('(Г)', '').replace(' ', '') if lv else ''
			except:
				pass

		data['size_d'] = ''
		data['size_h'] = ''
		data['size_w'] = ''

		if data['size']:
			try:
				list_size = data['size'].replace('х', 'x').split('x')
				data['size_w'] = list_size[0]
				data['size_h'] = list_size[1]
				data['size_d'] = list_size[2]
			except:
				pass

		try:
			features_block = cart.find('ul', class_="catalog-oneitem__feauters-block").find_all('li', class_="catalog-oneitem__feauter")
			for fb in features_block:
				data['dop_info'] += str('<p>'+fb.text+'</p>')
		except:
			pass

		# категория
		cur_parent = current_url.split('/')[-2] # reznye-portaly | portaly-s-kamnem и тд.
		if cur_parent == 'reznye-portaly' and (data['ochags'] or check_ochag): data['parent'] = 304
		elif cur_parent == 'reznye-portaly' and not data['ochags']: data['parent'] = 306
		elif cur_parent == 'portaly-s-kamnem' and (data['ochags'] or check_ochag): data['parent'] = 304
		elif cur_parent == 'portaly-s-kamnem' and not data['ochags']: data['parent'] = 309
		elif cur_parent == 'kaminy-classic-flame' and (data['ochags'] or check_ochag): data['parent'] = 304
		elif cur_parent == 'kaminy-classic-flame' and not data['ochags']: data['parent'] = 361
		elif cur_parent == 'ochagi': data['parent'] = 302
		else: continue

		# артикул
		art = str(fn.hashlib.md5(data['alias'].encode('utf-8')).hexdigest()[:6])
		data['article'] = 'IF-' + art


		data['vendor'] = 992
		data['published'] = 1
		data['template'] = 7
		data['color_id'] = data['color']
		# добавляем в csv файл
		write_csv(data)

		