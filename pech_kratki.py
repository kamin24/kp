import fun as fn
import re
import json
from multiprocessing import Pool

COLUMNS = [
    'parser',
    'title',
    'parent',
    'article',
    'alias',
    'published',
    'template',
    'vendor',
    'price',
    'old_price',
    'content',
    'gallery',
    'images',
    'power_hot',
    'hevolume',
    'size_d',
    'size_h',
    'size_w',
    'country',
    'dop_info',
    'color',
    'weight',
    'bfk_type_gas',
    'bfk_tip_nagrevaniya',
    'location',
    'bfk_diametr_dymohoda_mm',
    'type_id',
    'pdu',
    'color_id',
    'material_topki',
    'material_oblicovki',
    'open_doors',
    'Steklo',
    'chistoe_steklo',
    'Razvodka_vozduha',
    'Turbina',
    'Dojig_vozduha',
    'Kolosnik_i_zolnik',
    'Mesto_ustanovki',
    'Topka_insert',
    'Shirina_dvercy',
    'Vysota_dvercy',
    'Teplonakopitel',
    'Seria',
    'Balka',
    'Style',
    'Varochnaya_plita',
    'Duhovoy_shkaf',
    'Vyhod_dymohoda',
    'Dverca',
    'Forma_pechi',
    'seria_dvercy',
    'Ploschad_pomeschenia',
    'Vremya_gorenia',
    'material_pechi',
    'Obem_parnoy',
    'Massa_kamney',
    'Kamenka',
    'Vynosnoy_tunnel',
    'Bak',
    'GSM_modul',
    'Chislo_konturov',
    'Material_teploobmennika',
    'Variant_resheniya',
    'Kolichestvo_radiatorov',
    'Prohod_dymohoda',
    'Tolschina_izolyacii',
    'Diametr_vnutrenney_truby',
    'material_vnutrenney_truby',
    'Material_vneshney_truby',
    'tolcshina_vnutrenney_truby',
    'tolschina_vneshney_truby',
    'Tip_elementa',
    'Upravlenie',
    'obem_baka',
    'Teploobmennik_Registr',
    'toplivo',
    'documents',
    'Vodyanoy_kontur',
    'Podacha_vozduha',
    'youtube_id',
    'categories',
    'dymosbornik',
    'Pult',
    'Rejim_parnoi',
    'Vynos_portala',
    'Obem_parnoy_min',
    'Obem_parnoy_max',
    'diameter',
    'Diametr_vnesh_truby',
    'podkluchenie_dymohoda',
    'Shiber',
]


def create_csv():
    with open(fn.PATH + '/pech_kratki.csv', 'w', newline='', encoding='utf-8') as f:
        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow(COLUMNS)


def write_csv(data):
    with open(fn.PATH + '/pech_kratki.csv', 'a', newline='', encoding='utf-8') as f:
        writer = fn.csv.DictWriter(f, fieldnames=COLUMNS, delimiter=';')
        writer.writerow(data)


def change_csv(old, new):
    if fn.path.isfile(new):
        fn.remove(new)
    if fn.path.isfile(old):
        fn.rename(old, new)


name_resources = {}

list_columns = {}
base_site = 'https://www.pech.ru'

vendors = fn.get_vendors()
resourses = fn.get_resources()


def get_item(product):

    try:
        item_url = product[1]["url"]
        article = f'PCH-{product[0]}'
        parents = product[1]["parents"]

        print('{}{}'.format(base_site, item_url))

        data = list_columns.copy()

        data['parser'] = 'pech'
        data['article'] = article
        data['parent'] = parents[0]

        categories = []
        for p in parents:
            categories.append(resourses.get(str(p)))

        data['categories'] = "|".join(categories)

        cart_html = fn.get_html('{}{}'.format(base_site, item_url))

        if cart_html == False:
            return False

        cart = fn.BeautifulSoup(cart_html, 'html.parser')

        image_link = cart.find('link', {'href': True, 'rel': 'image_src'})

        if image_link:
            image_url = image_link.get('href')

        data['title'] = cart.find('h1').get_text().strip()
        data['alias'] = 'PCH-' + \
            fn.translate(data['title']) + \
            fn.translate(data['article']).replace('--', '')

        data['price'] = 0
        data['old_price'] = 0

        price = cart.find(
            'div', class_="item-form-info__price").get('data-price_default')
        old_price = cart.find(
            'span', class_="js-card-oldprice").text.replace(' ', '').replace('р', '')

        try:
            data['price'] = float(price)
        except:
            # по запросу
            data['price'] = 0

        try:
            data['old_price'] = old_price if old_price else 0
        except:
            data['old_price'] = 0

        videos = cart.find_all('div', class_="js-video-popup-frame")
        videos_id = []
        for video in videos:
            videos_id.append(video.get('data-youtube-video-id'))

        data['youtube_id'] = ''

        try:
            data['content'] = cart.find(
                'div', itemprop="description").prettify()
        except:
            data['content'] = ''

        if videos_id:
            data['youtube_id'] = videos_id[0]
            for video in videos_id:
                data['content'] += '<iframe width="450" height="300" src="https://www.youtube.com/embed/{}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'.format(
                    video)

        data['gallery'] = ''
        gallery = cart.find('div', class_="js-gallary-store").get('data-store')
        gallery_list = [base_site + i['SRC'] for i in json.loads(gallery)]

        data['gallery'] = '|'.join(gallery_list)
        data['images'] = ''

        if image_url:
            if 'www.pech.ru/' in image_url:
                data['images'] = '{}'.format(image_url)
            else:
                data['images'] = '{}{}'.format(base_site, image_url)
        elif gallery_list:
            data['images'] = gallery_list[0]

        try:
            documents = cart.find('div', id="documents").prettify()
        except:
            documents = ""

        data['documents'] = str(documents)

        characteristics = cart.find('div', id="characteristics")

        # try:
        items1 = characteristics.find_all(
            'div', class_=re.compile('col-lg-auto')
        )

        items2 = characteristics.find_all(
            'div', class_=re.compile('col-md-4')
        )

        items = []
        for item_char in items1:
            items = items + item_char.find_all('ul', class_="info-list")
        for item_char in items2:
            items = items + item_char.find_all('ul', class_="info-list")

        for item in items:
            unwanteds = item.find_all('div')
            for unwanted in unwanteds:
                unwanted.extract()
            data['dop_info'] += str(item)
            ul = item.find_all('li')
            for li in ul:
                spans = li.find_all('span')

                th = spans[0].text.strip()
                td = spans[len(spans)-1].text.strip()

                # print(f"{th}={td}")

                if 'Страна производства' in th:
                    data['country'] = td
                elif 'Производитель' in th:
                    ven = td.replace('\t', '').replace('\n', '')
                    if ven in vendors:
                        data['vendor'] = vendors[ven]
                    else:
                        new_ven = fn.get_vendor(ven)
                        data['vendor'] = new_ven['id'] if 'id' in new_ven else ""
                elif 'Высота, мм' in th:
                    data['size_h'] = td
                elif 'Глубина, мм' in th:
                    data['size_d'] = td
                elif 'Ширина, мм' in th:
                    data['size_w'] = td
                elif 'Мощность, квт' in th:
                    data['power_hot'] = td
                elif 'Водяной контур' in th:
                    data['Vodyanoy_kontur'] = td
                elif 'Объем помещения' in th:
                    data['hevolume'] = td
                elif 'Материал топки' in th:
                    data['material_topki'] = td
                elif 'Материал облицовки' in th:
                    data['material_oblicovki'] = td
                elif 'Цвет' in th:
                    data['color_id'] = td
                    data['color'] = "|".join(td.split(', ')) if td else ''
                elif 'Вес, кг' in th:
                    data['weight'] = float(td)
                elif 'Тип газа' in th:
                    data['bfk_type_gas'] = td
                elif 'Тип нагрева' in th:
                    data['bfk_tip_nagrevaniya'] = td
                elif 'Тип расположения' in th:
                    data['location'] = td
                elif 'Диаметр дымохода' in th:
                    data['bfk_diametr_dymohoda_mm'] = td
                elif th == 'Тип':
                    data['type_id'] = td
                elif 'Пульт' in th:
                    data['pdu'] = 1 if 'Нет' not in td else ''
                    data['Pult'] = td

                elif 'Дымосборник' in th:
                    data['dymosbornik'] = td
                elif 'Открывание двери' in th:
                    data['open_doors'] = td
                elif 'Стекло' in th:
                    data['Steklo'] = td
                elif 'Чистое стекло' in th:
                    data['chistoe_steklo'] = td
                elif 'Разводка воздуха по комнатам' in th:
                    data['Razvodka_vozduha'] = td
                elif 'Турбина' in th:
                    data['Turbina'] = td
                elif 'Дожиг воздуха' in th:
                    data['Dojig_vozduha'] = td
                elif 'Колосник и зольник' in th:
                    data['Kolosnik_i_zolnik'] = td
                elif 'Место установки' in th:
                    data['Mesto_ustanovki'] = td
                elif 'Топка-инсерт' in th:
                    data['Topka_insert'] = td
                elif 'Ширина дверцы, мм' in th:
                    data['Shirina_dvercy'] = td
                elif 'Высота дверцы, мм' in th:
                    data['Vysota_dvercy'] = td
                elif 'Теплонакопитель' in th:
                    data['Teplonakopitel'] = td
                elif 'Серия' == th:
                    data['Seria'] = td
                elif 'Балка' in th:
                    data['Balka'] = td
                elif 'Стиль' in th:
                    data['Style'] = td
                elif 'Варочная плита' in th:
                    data['Varochnaya_plita'] = td
                elif 'Духовой шкаф' in th:
                    data['Duhovoy_shkaf'] = td
                elif 'Выход дымохода' in th:
                    data['Vyhod_dymohoda'] = td
                elif 'Дверца' in th:
                    data['Dverca'] = td
                elif 'Форма печи' in th:
                    data['Forma_pechi'] = td
                elif 'Серия дверцы' in th:
                    data['seria_dvercy'] = td
                elif 'Площадь помещения, м2' in th:
                    data['Ploschad_pomeschenia'] = td
                elif 'Время горения, часов до' in th:
                    data['Vremya_gorenia'] = td
                elif 'Материал печи' in th:
                    data['material_pechi'] = td
                elif 'Объем парной, м3' in th:
                    data['Obem_parnoy'] = td
                elif 'Масса камней, кг' in th:
                    data['Massa_kamney'] = td
                elif 'Каменка' in th:
                    data['Kamenka'] = td
                elif 'Выносной туннель' in th:
                    data['Vynosnoy_tunnel'] = td
                elif 'Бак' == th:
                    data['Bak'] = td
                elif 'GSM модуль' in th:
                    data['GSM_modul'] = td
                elif 'Число контуров' in th:
                    data['Chislo_konturov'] = td
                elif 'Материал теплообменника' in th:
                    data['Material_teploobmennika'] = td
                elif 'Вариант решения' in th:
                    data['Variant_resheniya'] = td
                elif 'Количество радиаторов' in th:
                    data['Kolichestvo_radiatorov'] = td
                elif 'Проход дымохода' in th:
                    data['Prohod_dymohoda'] = td
                elif 'Толщина изоляции' in th:
                    data['Tolschina_izolyacii'] = td
                elif 'Диаметр внутренней трубы' in th:
                    data['Diametr_vnutrenney_truby'] = td
                elif 'Материал внутренней трубы' in th:
                    data['material_vnutrenney_truby'] = td
                elif 'Материал внешней трубы' in th:
                    data['Material_vneshney_truby'] = td
                elif 'Толщина внутренней трубы' in th:
                    data['tolcshina_vnutrenney_truby'] = td
                elif 'Толщина внешней трубы' in th:
                    data['tolschina_vneshney_truby'] = td
                elif 'Тип элемента' in th:
                    data['Tip_elementa'] = td
                elif 'Управление' in th:
                    data['Upravlenie'] = td
                elif 'Объем бака' in th:
                    data['obem_baka'] = td
                elif 'Теплообменник' in th:
                    data['Teploobmennik_Registr'] = td
                elif 'Топливо' in th:
                    data['toplivo'] = td
                elif 'Подача воздуха извне' in th:
                    data['Podacha_vozduha'] = td
                elif 'Режим парной' in th:
                    data['Rejim_parnoi'] = td
                elif 'Вынос портала' in th:
                    data['Vynos_portala'] = td
                elif 'Объем парной (min)' in th:
                    data['Obem_parnoy_min'] = td
                elif 'Объем парной (max)' in th:
                    data['Obem_parnoy_max'] = td
                elif 'Диаметр, мм' in th:
                    data['diameter'] = td
                elif 'Диаметр внешней трубы' in th:
                    data['Diametr_vnesh_truby'] = td
                elif 'Подключение дымохода' in th:
                    data['podkluchenie_dymohoda'] = td
                elif 'Шибер' in th:
                    data['Shiber'] = td
        # except Exception as e:
        #     print(e)

        if not data['Ploschad_pomeschenia'] and data['power_hot']:
            data['Ploschad_pomeschenia'] = 10 * float(data['power_hot'])

        if not data['old_price']:
            data['old_price'] = 0
        data['published'] = 0
        data['template'] = 7

        write_csv(data)
    except Exception as e:
        print(e)


def run():

    categories = [
        {
            "id": 5142,
            "url": "https://www.pech.ru/catalog/topki-kaminnye/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9006,
            "url": "https://www.pech.ru/catalog/kaminy-oblitsovki/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9008,
            "url": "https://www.pech.ru/catalog/portaly/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9009,
            "url": "https://www.pech.ru/catalog/gotovye-kaminy/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9010,
            "url": "https://www.pech.ru/catalog/kaminy-hitech/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 5144,
            "url": "https://www.pech.ru/catalog/pechi-kaminy/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 7283,
            "url": "https://www.pech.ru/catalog/bio-kaminy/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9012,
            "url": "https://www.pech.ru/catalog/gazovye-kaminy/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9013,
            "url": "https://www.pech.ru/catalog/otopitelnye-pechi/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9014,
            "url": "https://www.pech.ru/catalog/teplonakopitelnye-pechi/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9015,
            "url": "https://www.pech.ru/catalog/pechi-dlya-bani/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9016,
            "url": "https://www.pech.ru/catalog/elektricheskie-pechi-dlya-bani/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9017,
            "url": "https://www.pech.ru/catalog/oblitsovki-bannykh-pechey/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9018,
            "url": "https://www.pech.ru/catalog/parogeneratory/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9020,
            "url": "https://www.pech.ru/catalog/bani-i-teploobmenniki/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9019,
            "url": "https://www.pech.ru/catalog/drovyanye-i-ugolnye-grili/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9021,
            "url": "https://www.pech.ru/catalog/tandyr/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9022,
            "url": "https://www.pech.ru/catalog/gazovye-stoly-kaminy/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9023,
            "url": "https://www.pech.ru/catalog/ochagi-dlya-kostra/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9024,
            "url": "https://www.pech.ru/catalog/gazovye-grili/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9026,
            "url": "https://www.pech.ru/catalog/plity-silikata-kaltsiya/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9027,
            "url": "https://www.pech.ru/catalog/minerit/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9028,
            "url": "https://www.pech.ru/catalog/bazaltovaya-vata-i-karton/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9029,
            "url": "https://www.pech.ru/catalog/ognestoykie-plity-nichikha/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9030,
            "url": "https://www.pech.ru/catalog/napolnye-stekla/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9031,
            "url": "https://www.pech.ru/catalog/predtopochnye-listy/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9032,
            "url": "https://www.pech.ru/catalog/prochie-materialy-dlya-montazha/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9034,
            "url": "https://www.pech.ru/catalog/dymokhody-komplektami/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9035,
            "url": "https://www.pech.ru/catalog/ventilyatsiya/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9036,
            "url": "https://www.pech.ru/catalog/elementy-dymokhoda/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9038,
            "url": "https://www.pech.ru/catalog/kotly-tverdotoplivnye/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9039,
            "url": "https://www.pech.ru/catalog/gotovye-resheniya-sistemy-otopleniya/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9040,
            "url": "https://www.pech.ru/catalog/kotly-elektricheskie/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9041,
            "url": "https://www.pech.ru/catalog/vodonagrevateli/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9042,
            "url": "https://www.pech.ru/catalog/komponenti/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 9043,
            "url": "https://www.pech.ru/catalog/dopolnitelnoe-oborudovanie/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 10429,
            "url": "https://www.pech.ru/catalog/bannye-chany/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 10430,
            "url": "https://www.pech.ru/catalog/zashchitnye-i-ustanovochnye-elementy-dlya-pechey/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 10431,
            "url": "https://www.pech.ru/catalog/kamni-dlya-bani/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/?PAGESIZE=36000"
        },
        {
            "id": 10432,
            "url": "https://www.pech.ru/catalog/kotly-tverdotoplivnye/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/operaton-ruchnoe/?PAGESIZE=36000"
        },
        {
            "id": 10433,
            "url": "https://www.pech.ru/catalog/kotly-tverdotoplivnye/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/operaton-poluavtomat/?PAGESIZE=36000"
        },
        {
            "id": 10434,
            "url": "https://www.pech.ru/catalog/kotly-tverdotoplivnye/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/operaton-avtomat/?PAGESIZE=36000"
        },
        {
            "id": 10435,
            "url": "https://www.pech.ru/catalog/kotly-tverdotoplivnye/filter/manufacturer-abx-or-kratki-or-nordpeis-or-firebird-or-bef_home/fuel-pellety_granuly/?PAGESIZE=36000"
        }
    ]

    for col_key in COLUMNS:
        list_columns[col_key] = ""

    lists = {}

    for _, category in enumerate(categories):
        cur_cat = int(category['id'])
        print(cur_cat, category['url'])
        html = fn.get_html(category['url'])

        if not html:
            print('Not html: %s' % category['url'])
            continue

        soup = fn.BeautifulSoup(html, 'html.parser')

        for a in soup.find_all("div", class_="test_card"):
            atr = a.find("a", "js-favorites").get("data-id")
            if not atr:
                print('Not atr:')
                continue
            link = a.find("a", class_="thumb-card__title").get("href")
            lists.setdefault(atr, {})
            lists[atr].setdefault("parents", []).append(cur_cat)
            lists[atr]["url"] = link
            lists[atr]["parents"] = list(set(lists[atr]["parents"]))

    # atr = "45848"
    # cur_cat = 5142
    # lists.setdefault(atr, {})
    # lists[atr].setdefault("parents", []).append(cur_cat)
    # lists[atr]["url"] = "/catalog/topki-kaminnye/kaminnaya-topka-kratki-arke-75/"
    # print(lists)
    with open("lists_kratki.json", "w") as f:
        json.dump(lists, f)

    create_csv()

    with Pool(3) as p:
        print('pool-----------------')
        p.map(get_item, lists.items())
