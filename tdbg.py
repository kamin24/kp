import fun as fn

def create_csv():
	with open(fn.PATH + '/tdbg.csv', 'w', newline='', encoding='utf-8') as f:
		writer = fn.csv.writer(f, delimiter = ';')
		writer.writerow((
			'title',
			'content',
			'article',
			'alias',
			'vendor',
			'price',
			'size',
			'power_hot',
			'weight',
			'pdu',
			'color',
			'type_hearth',
			'heating_mode',
			'material',
			'hearth',
			'type_portal',
			'new',
			'sound',
			'template',
			'image',
			'gallery',
			'published',
			'parser',
			'parent',
			'color_id',
			'size_d',
			'size_h',
			'size_w',
			))

def write_csv(data):
	with open(fn.PATH + '/tdbg.csv', 'a', newline='', encoding='utf-8') as f:

		writer = fn.csv.writer(f, delimiter=';')
		writer.writerow((
			data['title'],
			data['content'],
			data['article'],
			data['alias'],
			data['vendor'],
			data['price'],
			data['size'],
			data['power_hot'],
			data['weight'],
			data['pdu'],
			data['color'],
			data['type_hearth'],
			data['heating_mode'],
			data['material'],
			data['hearth'],
			data['type_portal'],
			data['new'],
			data['sound'],
			data['template'],
			data['image'],
			data['gallery'],
			data['published'],
			'tdbg',
			data['parent'],
			data['color_id'],
			data['size_d'],
			data['size_h'],
			data['size_w'],
		))

def change_csv(old, new):
	if fn.path.isfile(new):
		fn.remove(new)
	if fn.path.isfile(old):
		fn.rename(old, new)
	

def tdbg():
	
	base_url = 'http://www.tdbg.ru'
	parser = fn.get_html('http://www.tdbg.ru/export/yml/data/tdbg-electrokamin.xml')

	xml = fn.BeautifulSoup(parser, "xml")

	offers = xml.find('offers').find_all('offer')

	# переименовываем старый файл
	change_csv(fn.PATH + '/tdbg.csv', fn.PATH + '/tdbg_prev.csv')
	# создаем новый файл csv
	create_csv()

	for offer in offers:
		data = {}
		# Категория
		parent = int(offer.find('categoryId').text)

		if parent == 120: data['parent'] = 26
		elif parent == 121 or parent == 234: data['parent'] = 2316
		elif parent == 124 or parent == 242: data['parent'] = 1474
		elif parent == 177 or parent == 236: data['parent'] = 2317
		elif parent == 125 or parent == 238 or parent == 239: data['parent'] = 369
		elif parent == 126 or parent == 240 or parent == 241: data['parent'] = 2315
		elif parent == 127 or parent == 237 or parent == 349: data['parent'] = 305
		elif parent == 150 or parent == 245 or parent == 379: data['parent'] = 304
		elif parent == 179 or parent == 243 or parent == 244: data['parent'] = 2276
		else: continue # пропускаем, т.к. у нас жесткая проверка по категориям

		# Название
		try:
			data['title'] = offer.find('model').text
		except:
			data['title'] = ''
		# Описание
		try:
			data['content'] = offer.find('description').text.strip()
		except:
			data['content'] = ''

		# артикул
		try:
			data['article'] = 'TD-'+offer.get('id')
		except:
			data['article'] = ''

		# алиас
		try:
			data['alias'] = fn.translate(data['title']) +'-'+ fn.translate(data['article'])
		except:
			data['alias'] = '' # все равно товар не добавится в базу если нет этого поля

		# Производитель
		try:
			vendor = offer.find('vendor').text
			if vendor == 'Royal Flame': data['vendor'] = 2
			else: data['vendor'] = 1003 # тут другого производителя и быть не может кроме этих двух
		except:
			data['vendor'] = 1003 # на всякий
		# Цена
		try:
			data['price'] = offer.find('price').text
		except:
			data['price'] = 0

		try:
			exit = 0
			data['size'] = ''
			data['size_d'] = ''
			data['size_h'] = ''
			data['size_w'] = ''
			data['power_hot'] = ''
			data['weight'] = ''
			data['pdu'] = ''
			data['color'] = ''
			data['type_hearth'] = ''
			data['heating_mode'] = ''
			data['material'] = ''
			data['hearth'] = ''
			data['type_portal'] = ''
			data['new'] = ''
			data['sound'] = ''
			data['compatible'] = ''
			for param in offer.find_all('param'):
				code = int(param.get('code'))
				if code == 93: # в xml файле творится неясная дичь с этим параметром, лучше такой товар не добавлять в базу
					exit = 1
					break
				text = param.text
				if code == 185:	
					data['size'] = text.replace('×', 'x').replace('х', 'x').replace(' мм', '').strip() if text else ''				

				elif code == 177 or code == 7300: data['power_hot'] = text + ' кВт' if param.get('unit') else ''
				elif code == 184: data['weight'] = text if text else ''
				elif code == 363: data['pdu'] = 1 if param.get('unit') == 'Да' else ''
				elif code == 1233: data['color'] = str(text) if text else 'Не задан'
				elif code == 4372: data['type_hearth'] = text if text else ''
				elif code == 4373: data['heating_mode'] = text if text else ''
				elif code == 1236: data['material'] = text if text else ''
				elif code == 4457: data['hearth'] = text if text else ''
				elif code == 5004: data['type_portal'] = text if text else ''
				elif code == 126: data['new'] = 1 if text == 'Да' else ''
				elif code == 5308: data['sound'] = 1 if text == 'Есть' else ''
				elif code == 129: 
					if param.get('id'):
						data['compatible'] = []
						data['compatible'].append('TD-'+param.get('id'))
				elif code == 4374 or code == 5012 or code == 1321 or code == 4371 or code == 5005: continue
				else: continue
		except:
			pass
		
		try:
			list_size = data['size'].split('x')
			data['size_w'] = list_size[0]
			data['size_h'] = list_size[1]
			data['size_d'] = list_size[2][:3]
		except:
			pass

		if exit and exit == 1:
			continue

		# изображения
		try:
			images = []
			for image in offer.find_all('picture'): images.append(base_url + image.text)

			data['image'] = images[0]
			data['gallery'] = '|'.join(images)	
		except:
			data['gallery'] = ''
			data['image'] = ''
		
		if not data['image'] and not data['gallery']:
			continue
		
		ignoreArt = ['TD-18974','TD-18971','TD-18976','TD-18975']
		data['published'] = 1
		if data['article'] in ignoreArt:
			data['published'] = 0
		
		data['template'] = 7
		data['color_id'] = data['color']
		# добавляем в csv файл
		write_csv(data)