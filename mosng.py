import fun as fn
import re


def create_csv():
    with open(fn.PATH + '/mosng.csv', 'w', newline='', encoding='utf-8') as f:
        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow((
            'title',
            'parent',
            'article',
            'alias',
            'price',
            'old_price',
            'content',
            'video_link',
            'gallery',
            'images',
            'type_id',
            'hearth',
            'type_hearth',
            'drova',
            'power_hot',
            'color',
            'color_id',
            'bfk_material_oblicovki',
            'install',
            'material',
            'bfk_option',
            'weight',
            'size',
            'size_d',
            'size_h',
            'size_w',
            'country',
            'published',
            'template',
            'vendor',
            'parser',
        ))


def write_csv(data):
    with open(fn.PATH + '/mosng.csv', 'a', newline='', encoding='utf-8') as f:

        writer = fn.csv.writer(f, delimiter=';')
        writer.writerow((
            data['title'],
            data['parent'],
            data['article'],
            data['alias'],
            data['price'],
            data['old_price'],
            data['content'],
            data['video_link'],
            data['gallery'],
            data['images'],
            data['type_id'],
            data['hearth'],
            data['type_hearth'],
            data['drova'],
            data['power_hot'],
            data['color'],
            data['color_id'],
            data['bfk_material_oblicovki'],
            data['install'],
            data['material'],
            data['bfk_option'],
            data['weight'],
            data['size'],
            data['size_d'],
            data['size_h'],
            data['size_w'],
            data['country'],
            data['published'],
            data['template'],
            data['vendor'],
            'mosng',
        ))


def change_csv(old, new):
    if fn.path.isfile(new):
        fn.remove(new)
    if fn.path.isfile(old):
        fn.rename(old, new)


def run():

    base_site = 'https://www.mosng.ru'
    html = fn.get_html(
        'https://www.mosng.ru/catalog/upload_catalog.php?SHOWALL_1=1&sflt=1&SECTION_ID=38')

    if html == False:
        return False

    soup = fn.BeautifulSoup(html, 'html.parser')
    list = soup.find('ul', class_='list-prod').find_all('li', class_='item')
    if not list:
        return False

    for li in list:
        data = {}
        cart_url = li.find('a', class_='product-link').get('href')
        # cart_url = '/catalog/opti-myst-bingham/'
        cart_html = fn.get_html('{}{}'.format(base_site, cart_url))
        print('{}{}'.format(base_site, cart_url))
        if cart_html == False:
            return False
        cart = fn.BeautifulSoup(cart_html, 'html.parser')

        category = cart.find('div', id="main").find(
            'header').find_all('a')[-1].get_text().lower()

        # категории
        if 'порталы' in category:
            parent = 304
        elif 'очаг' in category:
            parent = 302
        elif 'комплект' in category:
            parent = 2276
        elif 'печи' in category:
            parent = 1474
        else:
            parent = 26

        data['title'] = cart.find('h1').get_text().strip()
        data['parent'] = parent
        try:
            data['article'] = 'MNG-{}'.format(cart.find('dd',
                                                        itemprop="sku").get_text().strip())
        except:
            continue
        data['alias'] = 'MNG-' + \
            fn.translate(data['title']) + \
            fn.translate(data['article']).replace('--', '-')

        data['price'] = 0
        data['old_price'] = 0

        try:
            data['price'] = cart.find('div', itemprop="offers").find(
                'div', class_="price").find('span').get_text().replace(' ', '')
        except:
            data['price'] = 0

        try:
            data['old_price'] = cart.find('div', itemprop="offers").find(
                'div', class_="price").find('s').text.replace(' ', '')
        except:
            data['old_price'] = 0

        try:
            data['content'] = cart.find(
                'div', {'id': 'fld-desc'}).find('div', {"class": "content"})
        except:
            data['content'] = ''
        try:
            data['video_link'] = cart.find(
                'a', attrs={'data-video': True}).get('data-video')
        except:
            data['video_link'] = ''
        try:
            images = cart.find(
                'div', {"id": "prod-gallery"}).find_all('figure', itemprop="image")
        except:
            images = ''

        data['gallery'] = ''
        data['images'] = ''
        im = []
        if images:
            for image in images:
                im.append('{}{}'.format(base_site, image.get('content')))
            data['images'] = im[0]

        data['gallery'] = '|'.join(im)

        data['type_id'] = ''
        data['power_hot'] = ''
        data['type_hearth'] = ''
        data['drova'] = ''
        data['country'] = ''
        data['material'] = ''
        data['bfk_material_oblicovki'] = ''
        data['bfk_option'] = ''
        data['install'] = ''
        data['color'] = ''
        data['weight'] = ''
        data['hearth'] = ''
        data['size'] = ''
        data['size_d'] = ''
        data['size_h'] = ''
        data['size_w'] = ''
        data['vendor'] = ''

        vendors = {
            "dimplex": 1003,
            "royal flame": 2,
            "chazelles": 1023
        }

        try:
            items = cart.find('div', {'id': 'fld-tech'}
                              ).find('dl', class_='param').find_all('dt')

            for item in items:
                dt = item.get_text().lower()
                dd = item.find_next_sibling('dd')
                for br in dd.find_all('br')[:-1]:
                    br.replace_with('|')
                dd = dd.get_text().lower()

                if 'бренд' in dt:
                    data['vendor'] = vendors[dd] if dd in vendors else dd
                elif 'страна производителя' in dt:
                    data['country'] = dd
                elif 'мощность обогрева' in dt:
                    data['power_hot'] = dd if dd else ''
                elif 'материал портала' in dt:
                    data['bfk_material_oblicovki'] = dd if dd else ''
                elif 'эффект пламени' in dt:
                    data['drova'] = 1
                elif 'тип очага' in dt:
                    data['type_hearth'] = dd
                elif 'способ установки' in dt:
                    data['install'] = dd
                elif 'материал' in dt:
                    data['material'] = dd
                elif 'цвет' in dt:
                    data['color'] = dd if dd else 'Не задан'
                elif 'стиль' in dt:
                    data['type_id'] = dd
                elif 'опции' in dt:
                    data['bfk_option'] = dd
                elif 'вес' in dt:
                    data['weight'] = float(dd.replace(',', '.'))
                elif 'под очаг' in dt:
                    data['hearth'] = dd
                elif 'габариты' in dt:
                    data['size'] = re.sub(
                        r"\(.*\)", '', dd.replace(' ', '').replace('х', 'x').replace('×', 'x'))
                    try:
                        list_size = data['size'].split('x')
                        data['size_h'] = list_size[0]
                        data['size_w'] = list_size[1]
                        data['size_d'] = list_size[2] or ''
                    except:
                        pass
        except:
            pass

        if not data['old_price']:
            data['old_price'] = 0
        data['published'] = 1
        data['template'] = 7
        data['color_id'] = data['color']

        # добавляем в csv файл
        write_csv(data)
